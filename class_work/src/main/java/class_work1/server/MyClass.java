package class_work1.server;

import class_work1.MyAnno;

public class MyClass {
    @MyAnno(name = "field_INTEGER")
    private Integer field1 = 1;

    @MyAnno(name = "field_STRING")
    private String field2 = "sss";

    @MyAnno(name = "field_BOOLEAN")
    private Boolean field3 = true;

    @MyAnno(name = "field_INT")
    private int field4 = 5;

}