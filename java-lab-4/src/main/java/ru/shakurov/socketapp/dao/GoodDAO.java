package ru.shakurov.socketapp.dao;

import ru.shakurov.socketapp.models.Good;
import ru.shakurov.socketapp.utilities.ConnectionJDBC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GoodDAO {
    private Connection connection = ConnectionJDBC.getConnection();
    private final String GET_ALL = "SELECT * FROM good";
    private final String INSERT = "INSERT INTO good (name, price) VALUES (?,?)";
    private final String DELETE_BY_ID = "DELETE FROM good WHERE id = ?";

    public boolean insert(Good good) {
        try (PreparedStatement ps = connection.prepareStatement(INSERT)) {
            ps.setString(1, good.getName());
            ps.setInt(2, good.getPrice());
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteById(int id) {
        try (PreparedStatement ps = connection.prepareStatement(DELETE_BY_ID)) {
            ps.setInt(1, id);
            return ps.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Good> getAll() {
        List<Good> goods = new ArrayList<>();
        try (PreparedStatement ps = connection.prepareStatement(GET_ALL)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next())
                    goods.add(rowMapper.mapRow(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return goods;
    }

    private RowMapper<Good> rowMapper = row ->
            new Good().setId(row.getInt("id"))
                    .setName(row.getString("name"))
                    .setPrice(row.getInt("price"));
}
