package ru.shakurov.socketapp.communication;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import ru.shakurov.socketapp.jsonModels.payloads.*;
import ru.shakurov.socketapp.models.Good;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonReader {
    public static Object read(String jsonLine) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {

            ObjectNode objectNode = objectMapper.readValue(jsonLine, ObjectNode.class);
            JsonNode jsonNodeHeader = objectNode.get("header");
            if (jsonNodeHeader != null) {

                String header = jsonNodeHeader.asText();
                switch (header) {
                    case ("Message"):
                        return readMessage(objectNode);
                    case ("Login"):
                        return readLogin(objectNode);
                    case ("Show all"):
                        return readShowAll(objectNode);
                    case ("Add good"):
                        return readAddGood(objectNode);
                    case ("Delete good"):
                        return readDeleteGood(objectNode);
                    case ("Buy"):
                        return readBuy(objectNode);
                    case ("Token"):
                        return readToken(objectNode);
                }
            } else {
                JsonNode jsonNodeData = objectNode.get("data");
                if (jsonNodeData != null) {
                    return readData(objectNode);
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return null;
    }

    private static Object readToken(ObjectNode objectNode) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonPayload = objectNode.get("payload");
        TokenPayload tokenPayload = objectMapper.convertValue(jsonPayload, TokenPayload.class);
        return tokenPayload;
    }

    private static Object readBuy(ObjectNode objectNode) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonPayload = objectNode.get("payload");
        BuyPayload buyPayload = objectMapper.convertValue(jsonPayload, BuyPayload.class);
        return buyPayload;
    }

    private static Object readDeleteGood(ObjectNode objectNode) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonPayload = objectNode.get("payload");
        DeleteGoodPayload deleteGoodPayload = objectMapper.convertValue(jsonPayload, DeleteGoodPayload.class);
        return deleteGoodPayload;

    }

    private static Object readAddGood(ObjectNode objectNode) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonPayload = objectNode.get("payload");
        AddGoodPayload addGoodPayload = objectMapper.convertValue(jsonPayload, AddGoodPayload.class);
        return addGoodPayload;
    }

    private static Object readShowAll(ObjectNode objectNode) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonPayload = objectNode.get("payload");
        ShowAllPayload showAllPayload = objectMapper.convertValue(jsonPayload, ShowAllPayload.class);
        return showAllPayload;
    }

    private static MessagePayload readMessage(ObjectNode objectNode) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonPayload = objectNode.get("payload");
        MessagePayload messagePayload = objectMapper.convertValue(jsonPayload, MessagePayload.class);
        return messagePayload;
    }

    private static LoginPayload readLogin(ObjectNode objectNode) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonPayload = objectNode.get("payload");
        LoginPayload loginPayload = objectMapper.convertValue(jsonPayload, LoginPayload.class);
        return loginPayload;
    }

    private static List<Good> readData(ObjectNode objectNode) {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectNode.get("data");
        List<Good> data = new ArrayList<>();
        for (JsonNode node : jsonNode) {
            data.add(objectMapper.convertValue(node, Good.class));
        }
        return data;
    }
}
