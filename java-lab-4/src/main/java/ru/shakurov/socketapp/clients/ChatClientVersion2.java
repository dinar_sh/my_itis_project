package ru.shakurov.socketapp.clients;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.security.core.parameters.P;
import ru.shakurov.socketapp.communication.JsonParser;
import ru.shakurov.socketapp.communication.JsonReader;
import ru.shakurov.socketapp.jsonModels.payloads.MessagePayload;
import ru.shakurov.socketapp.jsonModels.payloads.TokenPayload;
import ru.shakurov.socketapp.models.Good;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Date;
import java.util.List;

public class ChatClientVersion2 {
    private Socket clientSocket;
    private BufferedReader reader;
    private PrintWriter writer;
    private String token;

    public void startConnection(String ip, Integer port) {
        try {
            clientSocket = new Socket(ip, port);
            writer = new PrintWriter(clientSocket.getOutputStream(), true);
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            token = null;
            new Thread(receiveMessage).start();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void sendMessage(String message) {
        String jsonLine = JsonParser.parse(message, token);
        if (jsonLine == null) {
            System.out.println("Неизвестная комманда");
        } else {
            writer.println(jsonLine);
        }
    }

    private Runnable receiveMessage = () -> {
        ObjectMapper objectMapper = new ObjectMapper();
        boolean isLogout = false;
        while (!isLogout) {
            try {
                String jsonLine = reader.readLine();
                ObjectNode objectNode = objectMapper.readValue(jsonLine, ObjectNode.class);

                if (objectNode.get("header") != null) {
                    String header = objectNode.get("header").asText();
                    switch (header) {
                        case ("Message"):
                            MessagePayload messagePayload = (MessagePayload) JsonReader.read(jsonLine);
                            System.out.println(messagePayload.getMessage());
                            if (messagePayload.getMessage().equals("До свидания"))
                                isLogout = true;
                            break;
                        case ("Token"):
                            TokenPayload tokenPayload = (TokenPayload) JsonReader.read(jsonLine);
                            this.token = tokenPayload.getToken();
                            break;
                    }

                } else {
                    if (objectNode.get("data") != null) {
                        List<Good> data = (List<Good>) JsonReader.read(jsonLine);
                        for (Good good : data) {
                            System.out.println(good.getId() + "     " + good.getName() + "      " + good.getPrice());
                        }
                    }
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    };
}
