package ru.shakurov.socketapp.models;

public class Good {
    private Integer id;
    private String name;
    private Integer price;

    public Integer getId() {
        return id;
    }

    public Good setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Good setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getPrice() {
        return price;
    }

    public Good setPrice(Integer price) {
        this.price = price;
        return this;
    }
}
