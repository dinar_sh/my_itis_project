package ru.shakurov.socketapp.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.shakurov.socketapp.dao.UserDAO;
import ru.shakurov.socketapp.models.User;
import ru.shakurov.socketapp.utilities.HashPassword;

import java.util.Optional;

public class AuthService {
    public static User auth(String login, String password) {
        Optional<User> userOptional = new UserDAO().findUserByLogin(login);
        if (userOptional.isEmpty()) return null;

        PasswordEncoder encoder = new BCryptPasswordEncoder();
        User user = userOptional.get();
        if (encoder.matches(password, user.getPassword())) {
            return user;
        }
        return null;
    }
}
