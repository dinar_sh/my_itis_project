package ru.shakurov.socketapp.services;

import ru.shakurov.socketapp.dao.GoodDAO;
import ru.shakurov.socketapp.jsonModels.Data;
import ru.shakurov.socketapp.models.Good;

import java.util.List;

public class GoodService {

    public Data<Good> showAll() {
        List<Good> goods = new GoodDAO().getAll();
        /*if (goods.size() > 0) {
            String all = "================================================\nid      name        price\n";

            for (Good good : goods) {
                all = good.getId() + "     " + good.getName() + "      " + good.getPrice() + "\n";
            }
            all += "================================================";
            return all;
        }
        return "================================================\n NOTHING TO SHOW \n ================================================";*/
        return new Data<Good>().setData(goods);
    }

    public boolean addGood(String name, Integer price) {
        return new GoodDAO().insert(new Good().setName(name).setPrice(price));
    }

    public boolean deleteGood(Integer id_good) {
        return new GoodDAO().deleteById(id_good);
    }
}
