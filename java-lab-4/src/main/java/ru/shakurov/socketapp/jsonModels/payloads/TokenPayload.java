package ru.shakurov.socketapp.jsonModels.payloads;

import ru.shakurov.socketapp.jsonModels.Payload;

public class TokenPayload extends Payload {
    private String token;

    public String getToken() {
        return token;
    }

    public TokenPayload setToken(String token) {
        this.token = token;
        return this;
    }
}
