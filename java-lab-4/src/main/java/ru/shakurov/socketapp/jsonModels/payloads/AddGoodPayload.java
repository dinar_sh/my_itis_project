package ru.shakurov.socketapp.jsonModels.payloads;

import ru.shakurov.socketapp.jsonModels.Payload;

public class AddGoodPayload extends Payload {
    private String goodName;
    private Integer price;

    public String getGoodName() {
        return goodName;
    }

    public AddGoodPayload setGoodName(String goodName) {
        this.goodName = goodName;
        return this;
    }

    public Integer getPrice() {
        return price;
    }

    public AddGoodPayload setPrice(Integer price) {
        this.price = price;
        return this;
    }
}
