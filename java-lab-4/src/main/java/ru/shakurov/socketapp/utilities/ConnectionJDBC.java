package ru.shakurov.socketapp.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionJDBC {
    private static Connection connection;
    private static final String URL_ADDRESS = "jdbc:mysql://localhost:3306/java_lab_1?serverTimezone=Europe/Moscow";
    private static final String USER = "root";
    private static final String PASSWORD = "ifrehjdlbyfh";

 /*   private static Properties properties;

    public void setProperties(Properties properties){
        this.properties = properties;
    }*/

    public static Connection getConnection() {
        if (connection != null) {
            return connection;
        } else {
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                connection = DriverManager.getConnection(URL_ADDRESS, USER, PASSWORD);
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return connection;
        }
    }
}
