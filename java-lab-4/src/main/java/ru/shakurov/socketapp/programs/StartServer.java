package ru.shakurov.socketapp.programs;

import ru.shakurov.socketapp.servers.ChatServerVersion2;

public class StartServer {
    public static void main(String[] argv) {
        ChatServerVersion2 chatServer = new ChatServerVersion2();
        chatServer.start(7000);
    }
}
