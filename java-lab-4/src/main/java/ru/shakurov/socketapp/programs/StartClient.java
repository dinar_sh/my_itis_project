package ru.shakurov.socketapp.programs;


import ru.shakurov.socketapp.clients.ChatClientVersion2;
import ru.shakurov.socketapp.communication.JsonParser;

import java.util.Scanner;

public class StartClient {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Для того, чтобы войти надо залогиниться. Пример: \"/login user@mail.ru qwerty007\"");
        System.out.println("Для того, чтобы выйти напишите: \"/logout\"");
        System.out.println("покупка: /buy *id_good*");
        System.out.println("добавить: /add *good_name*  *good_price*");
        System.out.println("удалить: /delete *id_good*");
        System.out.println("посмотреть всё: /show_all");
        System.out.println();
        System.out.println();
        System.out.println();

        ChatClientVersion2 chatClient = new ChatClientVersion2();
        chatClient.startConnection("127.0.0.1", 7000);

        System.out.println("Залогиньтесь");


        while (true) {
            chatClient.sendMessage(sc.nextLine());
        }
    }
}
