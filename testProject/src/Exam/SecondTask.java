package Exam;

import java.util.Arrays;

public class SecondTask {
    public static void main(String[] args) {
        int[] array = new int[3];
        array[0] = 1;
        array[1] = 2;
        array[2] = 3;
        someMethod(array);
    }

    public static void someMethod(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int sum = Arrays.stream(Arrays.copyOfRange(array, i, array.length)).reduce(0, (o1, o2) -> o1 + o2);
            System.out.println(sum);
        }
    }
}
