package Exam.second;

public class Monitor {
    Integer currentSum;

    public Monitor(Integer currentSum) {
        this.currentSum = currentSum;
    }

    public void add(Integer toAdd){
        currentSum += toAdd;
    }
}
