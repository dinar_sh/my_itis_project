package Exam.second;

import java.util.Arrays;

public class MyThread extends Thread {
    int[] arr;
    int startIndex;
    int endIndex;
    Monitor monitor;

    public MyThread(int[] arr, int endIndex, Monitor monitor) {
        this.arr = arr;
        this.startIndex = 0;
        this.endIndex = endIndex;
        this.monitor = monitor;
    }

    @Override
    public void run() {
        synchronized (monitor) {
                int summ = Arrays.stream(Arrays.copyOfRange(arr, 0, endIndex+1)).reduce(0, (o1, o2) -> o1 + o2);
                System.out.println(summ);
                monitor.add(summ);
                System.out.println("currentSum = " + monitor.currentSum);
        }
    }
}
