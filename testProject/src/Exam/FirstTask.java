package Exam;

import java.util.*;
import java.util.stream.Collectors;

public class FirstTask {
    public static void main(String[] args) {
        ArrayList<Integer> al = new ArrayList<>();

        al.add(123);
        al.add(102);

        Collections.sort(al, (o1, o2) -> {
                    o1 = Math.abs(o1);
                    o2 = Math.abs(o2);
                    String o1String = o1.toString();
                    String o2String = o2.toString();
                    int firstSum = 0;
                    int firstMul = 1;
                    int secondSum = 0;
                    int secondMul = 1;
                    for (int i = 0; i < o1String.length(); i++) {
                        firstMul *= Integer.parseInt(o1String.substring(i, i + 1));
                        firstSum += Integer.parseInt(o1String.substring(i, i + 1));
                    }
                    for (int i = 0; i < o2String.length(); i++) {
                        secondMul *= Integer.parseInt(o2String.substring(i, i + 1));
                        secondSum += Integer.parseInt(o2String.substring(i, i + 1));
                    }
                    int firstAbs = Math.abs(firstMul - firstSum);
                    int secondAbs = Math.abs(secondMul - secondSum);
                    return secondAbs - firstAbs;
                }
        );
        System.out.println(al);

    }

    public static void someMethod(Collection<Integer> collection) {
        Integer[] arr = (Integer[]) collection.toArray();
        collection = collection.stream().sorted((o1, o2) -> {
            o1 = Math.abs(o1);
            o2 = Math.abs(o2);
            String o1String = o1.toString();
            String o2String = o2.toString();
            int firstSum = 0;
            int firstMul = 1;
            int secondSum = 0;
            int secondMul = 1;
            for (int i = 0; i < o1String.length(); i++) {
                firstMul *= Integer.parseInt(o1String.substring(i, i + 1));
                firstSum += Integer.parseInt(o1String.substring(i, i + 1));
            }
            for (int i = 0; i < o2String.length(); i++) {
                secondMul *= Integer.parseInt(o2String.substring(i, i + 1));
                secondSum += Integer.parseInt(o2String.substring(i, i + 1));
            }
            int firstAbs = Math.abs(firstMul - firstSum);
            int secondAbs = Math.abs(secondMul - secondSum);
            return secondAbs - firstAbs;
        }).collect(Collectors.toList());

    }

    public static void newMethod(Collection<Integer> collection) {
        Integer[] arr = (Integer[]) collection.toArray();
        Arrays.sort(arr,(o1,o2)->{
            o1 = Math.abs(o1);
            o2 = Math.abs(o2);
            String o1String = o1.toString();
            String o2String = o2.toString();
            int firstSum = 0;
            int firstMul = 1;
            int secondSum = 0;
            int secondMul = 1;
            for (int i = 0; i < o1String.length(); i++) {
                firstMul *= Integer.parseInt(o1String.substring(i, i + 1));
                firstSum += Integer.parseInt(o1String.substring(i, i + 1));
            }
            for (int i = 0; i < o2String.length(); i++) {
                secondMul *= Integer.parseInt(o2String.substring(i, i + 1));
                secondSum += Integer.parseInt(o2String.substring(i, i + 1));
            }
            int firstAbs = Math.abs(firstMul - firstSum);
            int secondAbs = Math.abs(secondMul - secondSum);
            return secondAbs - firstAbs;
        });
        collection.clear();
        for (Integer i:
             arr) {
            collection.add(arr[i]);
        }
    }
}
