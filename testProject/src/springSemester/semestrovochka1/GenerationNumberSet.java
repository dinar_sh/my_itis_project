package springSemester.semestrovochka1;

import fallSemester.finalExam.MyRandom;

import java.io.FileWriter;
import java.io.IOException;

public class GenerationNumberSet {
    public static void main(String[] args) throws IOException {
        FileWriter fileWriter = new FileWriter("input.txt");
        int length = 100;

        for (int i = 0; i < 90; i++) {
            for (int j = 0; j < length; j++) {
                int val = MyRandom.getRandom(-10000, 10000);
                fileWriter.write(Integer.toString(val) + " ");
            }

            length += 100;
            fileWriter.write("\n");
        }

        fileWriter.close();
    }
}
