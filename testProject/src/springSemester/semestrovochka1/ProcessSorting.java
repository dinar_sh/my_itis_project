package springSemester.semestrovochka1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class ProcessSorting {
    public static void main(String[] args) throws FileNotFoundException {
        File f1 = new File("input.txt");
        Scanner scanner = new Scanner(f1);
        File f2 = new File("output.txt");
        PrintWriter printWriter = new PrintWriter(f2);

        File f4 = new File("output4.txt");
        PrintWriter printWriter4 = new PrintWriter(f4);

        File f3 = new File("outputList.txt");
        PrintWriter printWriter2 = new PrintWriter(f3);

        long[][] arrTime = new long[2][90];
        long[][] listTime = new long[2][90];
        int countArr = 0;
        int countList = 0;

        for (int i = 0; i < 90; i++) {
            String str = scanner.nextLine();
            String[] strArr = str.split(" ");
            int[] arr = parseintArr(strArr);
            LinkedList<Integer> list = parseintList(strArr);


            long startTime = System.nanoTime();
            CombSort.sort(arr);
            long endTime = System.nanoTime();
            arrTime[0][countArr] = arr.length;
            arrTime[1][countArr++] = endTime - startTime;


            long startTimeList = System.nanoTime();
            CombSort.sort(list);
            long endTimeList = System.nanoTime();
            listTime[0][countList] = list.size();
            listTime[1][countList++] = endTimeList - startTimeList;


            for (int j = 0; j < arr.length; j++) {
                printWriter.print(arr[j] + " ");
            }
            printWriter.print("\n");


            while (list.size() != 0) {
                printWriter2.write(list.removeFirst() + " ");
            }
            printWriter2.print("\n");
        }
        printWriter.close();
        printWriter2.close();
        System.out.println(Arrays.toString(arrTime[0]));
        System.out.println(Arrays.toString(arrTime[1]));

        System.out.println(Arrays.toString(listTime[0]));
        System.out.println(Arrays.toString(listTime[1]));


        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 90; j++) {
                printWriter4.print(arrTime[i][j] + " ");
            }
            printWriter4.println();
        }

        printWriter4.println();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 90; j++) {
                printWriter4.print(listTime[i][j] + " ");
            }
            printWriter4.println();
        }
        printWriter4.close();

    }


    private static int[] parseintArr(String[] str) {
        int[] arr = new int[str.length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(str[i]);
        }
        return arr;
    }

    private static LinkedList<Integer> parseintList(String[] str) {
        LinkedList<Integer> arr = new LinkedList<>();
        for (int i = 0; i < str.length; i++) {
            arr.add(Integer.parseInt(str[i]));
        }
        return arr;
    }
}
