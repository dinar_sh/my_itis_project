package springSemester.lesson10new.lambdas;

public class Main {
    public static void main(String[] args) {
        MyStrings ms = new MyStrings();
        ms.add("aaaa");
        ms.add("qwertyiu");
        MyFunction mf = new MyFunction() {
            @Override
            public String process(String s) {
                return s.toUpperCase();
            }
        };
        MyFunction myFunctionWithLamda = s -> s.toUpperCase();
        ms.processAll(myFunctionWithLamda);

        ms.showAll();
    }
}
