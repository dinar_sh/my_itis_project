package springSemester.lesson10new.hometask;

import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        MyArrayList<String> st = new MyArrayList<>();
        st.add("sdf");
        st.add("adf");
        st.add("sdf");
        st.add("sdf");


        System.out.println(st.stream()
                .map(s -> s.toUpperCase())
                .collect(Collectors.toList()));

        System.out.println(st.stream()
                .filter(s -> s.charAt(0) == 'a')
                .collect(Collectors.toList()));
    }
}
