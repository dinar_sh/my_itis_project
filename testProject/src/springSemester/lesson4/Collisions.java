package springSemester.lesson4;

import fallSemester.rpg.RandomRange;

import java.util.Arrays;
import java.util.UUID;

public class Collisions {
    public static void main(String[] args) {
        int[] arr = new int[16];
        for (int i = 0; i < 10000; i++) {
            String s = getRandomString();
            int hashcode = s.hashCode();
            int index = Math.abs(hashcode % 16);
            arr[index]++;

        }

        System.out.println(Arrays.toString(arr));
    }

    static String getRandomString() {
        int i = RandomRange.getRandom(0, 1000);
        String str = "";
        for (int j = 0; j < i; j++) {
            str += (char) RandomRange.getRandom(0, 150);
        }

        return UUID.randomUUID().toString();
    }
}
