package springSemester.lesson14;

import fallSemester.finalExam.MyRandom;

public class MainApp {
    public static long sum;

    public static void main(String[] args) throws InterruptedException {
        int[] arr = new int[1_000_000];
        long sum1 = 0;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = MyRandom.getRandom(1, 1000);
            sum1 += arr[i];
        }
        CounterThread c1 = new CounterThread(0, 200_000, arr);
        CounterThread c2 = new CounterThread(200_000, 400_000, arr);
        CounterThread c3 = new CounterThread(400_000, 600_000, arr);
        CounterThread c4 = new CounterThread(600_000, 800_000, arr);
        CounterThread c5 = new CounterThread(800_000, 1_000_000, arr);


        c1.start();


        c2.start();

        c3.start();

        c4.start();


        c5.start();

        c1.join();
        c2.join();
        c3.join();
        c4.join();
        c5.join();

        System.out.println(sum);
        System.out.println(sum1);
    }
}
