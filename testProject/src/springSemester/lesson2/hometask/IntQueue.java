package springSemester.lesson2.hometask;

public class IntQueue {
    private Node first;
    private Node last;
    private int size;

    public IntQueue() {
    }

    public int size() {
        return size;
    }

    public boolean isEmtpty() {
        return size == 0;
    }

    public int first() {
        return first.value;
    }

    public void enqueue(int elem) {
        if (size == 0) {
            first = last = new Node(elem);
            size++;
        } else {
            Node newNode = new Node(elem);
            last.next = newNode;
            last = newNode;
            size++;
        }
    }

    public int dequeue() {
        if (size == 0) {
            throw new IllegalStateException("Queue already is empty.");
        } else {
            int toReturn = first.value;
            first = first.next;
            size--;
            return toReturn;
        }
    }
}
