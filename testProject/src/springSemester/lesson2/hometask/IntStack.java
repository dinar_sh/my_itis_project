package springSemester.lesson2.hometask;

/**
 * based on linked list
 */
public class IntStack {
    private Node last;
    private int size;

    public IntStack() {
    }

    public void push(int elem) {
        if (last == null) {
            last = new Node(elem);
        } else {
            Node newNode = new Node(elem);
            Node current = last;
            last = newNode;
            newNode.next = current;
        }
        size++;
    }

    public int pop() {
        if (size == 0) {
            throw new IllegalStateException("Stack is empty");
        } else {
            int i = last.value;
            last = last.next;
            size--;
            return i;
        }
    }

    public int top() {
        return last.value;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }


}
