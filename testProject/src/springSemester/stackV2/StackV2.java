package springSemester.stackV2;

public class StackV2 {
    private final static int INITIAL_CAPACITY = 10;
    public static int number;
    private char[] arr;
    private int n;

    public StackV2() {
        arr = new char[INITIAL_CAPACITY];
        n = 0;
    }

    public void push(char c) {
        if (n == arr.length) {
            //"расширить" массив - завести новый в 1.5 раза больше
            char[] arr2 = new char[arr.length + (arr.length >> 1)];
            for (int i = 0; i < arr.length; i++) {
                arr2[i] = arr[i];
            }
            arr = arr2;
        }
        arr[n++] = c;
    }

    public char pop() {
        if (n == 0) {
            throw new IllegalStateException("IntStack is empty");
        }
        return arr[--n];
    }

    public boolean isEmpty() {
        return n == 0;
    }
}
