package springSemester.lists;

public interface IntList {
    /**
     * Adds number to the end of the list
     *
     * @param elem number to add
     */
    void add(int elem);

    void add(int elem, int position);

    /**
     * Returns the element with specified index
     */
    int get(int index);

    int remove(int index);

    //возвращает кол-во элементов в списке
    int size();

    //возвращает содержимое списка в виде массива
    int[] toArray();

    //упорядочивает числа в списке по возрастанию
    void sort();

    //вставляет в данный список все элементы из list, начиная с posistion в данном списке
    void addAll(IntList list, int position);

    //индекс последнего вхождения данного элемента в списке
    int lastIndexOf(int elem);
}
