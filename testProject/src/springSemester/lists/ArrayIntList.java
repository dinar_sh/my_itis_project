package springSemester.lists;

public class ArrayIntList implements IntList {
    private final static int INITIAL_CAPACITY = 10;
    private int[] arr;
    private int numberElements;

    public ArrayIntList() {
        this.numberElements = 0;
        this.arr = new int[INITIAL_CAPACITY];

    }

    @Override
    public void add(int elem) {
        if (numberElements == arr.length) {
            int[] newArray = new int[arr.length + arr.length >> 1];
            for (int i = 0; i < arr.length; i++) {
                newArray[i] = arr[i];
            }
            arr = newArray;
        }
        arr[numberElements++] = elem;
    }

    @Override
    public void add(int elem, int position) {
        if (position > numberElements || position < 0) {
            throw new ArrayIndexOutOfBoundsException("Impossible add element at position \"" + position + "\"");
        }
        if (numberElements == arr.length) {
            int[] newArray = new int[arr.length + arr.length >> 1];
            for (int i = 0; i < position; i++) {
                newArray[i] = arr[i];
            }
            newArray[position] = elem;

            for (int i = arr.length - 1; i > position - 1; i--) {
                newArray[i + 1] = arr[i];
            }

            arr = newArray;
        } else {
            for (int i = arr.length - 1; i > position - 1; i--) {
                arr[i + 1] = arr[i];
            }
            arr[position] = elem;
        }
        numberElements++;
    }

    @Override
    public int get(int index) {
        if (index >= numberElements || index < 0) {
            throw new ArrayIndexOutOfBoundsException("No element at position \"" + index + "\"");
        }
        return arr[index];
    }

    @Override
    public int remove(int index) {
        if (index >= numberElements || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Impossible remove element from position \"" + index + "\"");
        }
        int removedElement = arr[index];
        for (int i = index; i < arr.length - 1; i++) {
            arr[i] = arr[i + 1];
        }
        numberElements--;
        return removedElement;
    }

    @Override
    public int size() {
        return numberElements;
    }

    @Override
    public int[] toArray() {
        return arr.clone();
    }

    @Override
    public void sort() {
        for (int i = 0; i < arr.length - 1; i++) {
            int min = i;

            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[min])
                    min = j;
            }

            int temp = arr[min];
            arr[min] = arr[i];
            arr[i] = temp;
        }
    }

    @Override
    public void addAll(IntList list, int position) {
        int[] addArray = list.toArray();
        if (position <= numberElements && position >= 0) {
            if (arr.length - numberElements < list.size()) {
                int[] newArray = new int[arr.length + list.size() + 10];
                int i;
                for (i = 0; i < position; i++) {
                    newArray[i] = arr[i];
                }
                for (int j = 0; j < list.size(); j++) {
                    newArray[i + j + 1] = addArray[j];
                }
                i = i + list.size();
                for (int j = 0; j < numberElements - position; j++) {
                    newArray[i + j + 1] = arr[j + position];
                }
                arr = newArray;
                numberElements = numberElements + list.size();
            } else {
                for (int i = numberElements - 1; i >= position; i--) {
                    arr[i + list.size()] = arr[i];
                }
                for (int i = position; i < list.size(); i++) {
                    arr[i] = addArray[i - position];
                }
                numberElements = numberElements + list.size();
            }
        } else {
            throw new ArrayIndexOutOfBoundsException("Impossible add element list at position \"" + position + "\"");
        }
    }

    @Override
    public int lastIndexOf(int elem) {

        for (int i = arr.length - 1; i >= 0; i--) {
            if (elem == arr[i])
                return i;
        }
        System.out.println(elem + " Has not been found.");
        return -1;                                              //не был уверен, нужно ли кидать эксепшн

    }


    public IntIterator iterator() {
        return new IntIteratorImpl();
    }

    class IntIteratorImpl implements IntIterator {
        int count = 0;

        public IntIteratorImpl() {
        }

        @Override
        public boolean hasNext() {
            return count < numberElements;
        }

        @Override
        public int next() {
            return arr[count++];
        }
    }
}
