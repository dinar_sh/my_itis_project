package springSemester.lists;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class    ArrayIntListTest {

    private ArrayIntList arrayIntList;

    @Before
    public void setUp() {
        this.arrayIntList = new ArrayIntList();
    }

    @Test
    public void testSizeAfterAdding() {
        arrayIntList.add(12);

        assertEquals(1, arrayIntList.size());
    }

    @Test
    public void testSizeAfterDeleting() {
        arrayIntList.add(12);
        arrayIntList.remove(0);
        assertEquals(0, arrayIntList.size());
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testArrayIndexOutOfBounds() {
        arrayIntList.add(12);
        arrayIntList.remove(1);
    }

    @Test
    public void testLastIndexOf() {
        arrayIntList.add(12);
        arrayIntList.add(12);
        arrayIntList.add(13);
        arrayIntList.add(12);
        arrayIntList.add(14);
        assertEquals(3, arrayIntList.lastIndexOf(12));
    }

    @Test
    public void testSort() {
        arrayIntList.add(12);
        arrayIntList.add(1);
        arrayIntList.add(-3);
        arrayIntList.add(0);
        arrayIntList.add(12);
        arrayIntList.add(10);
        arrayIntList.sort();

        for (int i = 0; i < arrayIntList.size() - 2; i++) {
            if (arrayIntList.get(i) > arrayIntList.get(i + 1)) {
                fail();
            }
        }
    }

    @Test
    public void testRemove() {
        arrayIntList.add(12);

        assertEquals(12, arrayIntList.remove(0));
    }

    @Test
    public void testToArray() {
        arrayIntList.add(12);
        arrayIntList.add(13);
        arrayIntList.add(14);
        arrayIntList.add(15);
        int[] arr = arrayIntList.toArray();
        for (int i = 0; i < arrayIntList.size(); i++) {
            if (arr[i] != arrayIntList.get(i)) {
                fail();
            }
        }

    }
}
