package springSemester.lists;

public interface IntIterator {
    boolean hasNext();

    int next();
}
