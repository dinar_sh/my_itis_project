package springSemester.lesson12.superbestawesomeframework;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SuperBestAwesomeFramework {
    public static <T> List<T> getMany(Class<T> c, int count) {
        List<T> list = new ArrayList<>();
        try {
            for (int i = 0; i < count; i++) {
                list.add(c.newInstance());
            }
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        return list;
    }

    public static <T> List<T> getMany(Class<T> aClass, int count, Object... params) {
        List<T> list = new ArrayList<>();
        Class[] classes = new Class[params.length];

/*
         Class[] classList = (Class[]) Arrays.stream(params).map(s -> s.getClass()).toArray();
*/


        Class[] classList = Arrays.stream(params).map(s -> s.getClass()).toArray(size -> new Class[size]);

/*        for (int i = 0; i < params.length; i++) {
            classes[i] = params[i].getClass();
        }
*/
        try {
            Constructor<T> constructor = aClass.getConstructor(classList);
            for (int i = 0; i < count; i++) {
                try {
                    list.add(constructor.newInstance(params));
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return list;

// =========================================================================================
/*        try {
            Constructor<T> constructor = aClass.getConstructor(int.class, String.class);
            for (int i = 0; i < count; i++) {
                try {
                    list.add(constructor.newInstance(params));
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return list;
    }*/

    }
}
