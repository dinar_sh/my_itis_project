package springSemester.lesson12.superbestawesomeframework;

import java.util.List;

public class Main {
    public static void main(String[] args) {
/*
        List<SimpleStudent> list = SuperBestAwesomeFramework.getMany(SimpleStudent.class, 10);
*/
        List<SimpleStudent> list = SuperBestAwesomeFramework.getMany(SimpleStudent.class, 100, 18, "John");
        for (SimpleStudent simpleStudent :
                list) {
            System.out.println(simpleStudent);
        }

    }
}
