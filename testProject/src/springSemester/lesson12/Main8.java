package springSemester.lesson12;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Main8 {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<Student> c = Student.class;
        Constructor<Student> constructor = c.getConstructor(int.class);
        Student s = constructor.newInstance(12);
        System.out.println(s);

    }
}
