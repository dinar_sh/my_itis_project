package springSemester.lesson5;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        int[] arr = gerRandomArr(1000_000);
        int[] arr1 = Arrays.copyOf(arr, arr.length);
        int[] arr2 = Arrays.copyOf(arr, arr.length);
        long start = System.nanoTime();
        MergeSort.mergeSort(arr1);
        long end = System.nanoTime();
        System.out.println(Arrays.toString(arr1));
        System.out.println(end - start);


        long start1 = System.nanoTime();
        selectionSort(arr2);
        long end1 = System.nanoTime();
        System.out.println(Arrays.toString(arr1));
        System.out.println(end1 - start1);
    }

    private static int[] gerRandomArr(int size) {
        int[] arr = new int[size];
        Random r = new Random();
        for (int i = 0; i < size; i++) {
            arr[i] = r.nextInt(100_000);
        }
        return arr;
    }

    private static void selectionSort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int indexOfMax = 0;
            for (int j = 0; j < arr.length - i; j++) {

                if (arr[indexOfMax] < arr[j]) {
                    indexOfMax = j;
                }
            }

            int b = arr[indexOfMax];
            arr[indexOfMax] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = b;
        }
    }
}
