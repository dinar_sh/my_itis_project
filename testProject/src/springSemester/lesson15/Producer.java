package springSemester.lesson15;

public class Producer extends Thread {
    private Product product;

    public Producer(Product product) {
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                while (!product.isConsumed()) {
                    System.out.println("Ещё не потреблен!");
                    try {
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }

                }
                product.produce();
                System.out.println("Прозведён");
                product.notify();
            }
        }
    }
}
