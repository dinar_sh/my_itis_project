package springSemester.lesson6;

//функциональны интерфейс с ровно одним абстрактным методом

public interface ProcessingRule {
    String process(String s);
}
