package springSemester.brackets;

public class StackTestMain {
    public static void main(String[] args) throws IllegalAccessException {
        Stack stack = new Stack(10);
        stack.push('1');
        stack.push('2');
        System.out.println(stack.pop());
        System.out.println(stack.pop());

        String str = "hello";
        char[] ar = str.toCharArray();

        for (int i = 0; i < ar.length; i++) {
            stack.push(ar[i]);
        }

        for (int i = 0; i < ar.length; i++) {
            System.out.println(stack.pop());
        }
    }
}
