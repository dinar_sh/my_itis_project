package springSemester.brackets;

import java.util.Scanner;

public class BracketsTest {
    public static void main(String[] args) {
        // {[(5+3)*8+8]-7*[3+5*6]}
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        boolean b = areBracketsCorrect(s);
        System.out.println(b);
    }

    static boolean areBracketsCorrect(String str) {
        Stack stack = new Stack(100);
        char[] symbols = str.toCharArray();
        for (char c : symbols) {
            if (c == '(' || c == '[' || c == '{') {
                stack.push(c);
            } else {
                if (c == ')' || c == ']' || c == '}') {
                    if (!stack.isEmpty()) {
                        char ch = stack.pop();
                        if ((ch == '(' && c == ')') || (ch == '[' && c == ']') || (ch == '{' && c == '}')) {
                        } else
                            return false;
                    } else
                        return false;
                }

            }
        }
        return stack.isEmpty();
    }
}
