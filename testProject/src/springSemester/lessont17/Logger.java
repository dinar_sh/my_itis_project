package springSemester.lessont17;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

public class Logger {
    private final static String FILE_NAME = "log.txt";
    private PrintWriter writer;

    public Logger() {
        File f = new File(FILE_NAME);
        try {
            writer = new PrintWriter(f);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException("Unable to create logger");
        }
    }

    void info(String message) {
        writer.println("INFO: " + LocalDateTime.now() + " " + message);
    }

    void error(String message){
        writer.println("ERROR: " + LocalDateTime.now() + " " + message);

    }
}
