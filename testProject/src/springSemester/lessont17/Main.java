package springSemester.lessont17;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        TextProvider provider1 = new SimpleTextProvider("Мама рама");
        File f = new File(("someText.txt"));
        FileTextProvider ftp = new FileTextProvider(f);
        TextAnalyzer analyzer = new JaccardTextAnalyzer();
        double coef = analyzer.analyze(provider1, new FileWithNameTextProvider(ftp));
        System.out.println(coef);
    }
}
