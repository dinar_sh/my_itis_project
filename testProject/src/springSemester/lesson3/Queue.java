package springSemester.lesson3;

public class Queue<E> {
    private Node first;
    private Node last;

    public void enqueue(E ele) {
        Node oldLast = last;
        last = new Node();
        last.value = ele;
        last.next = null;
        if (isEmpty()) {
            first = last;
        } else {
            oldLast.next = last;
        }
    }

    public E dequeue() {
        if (first == null) {
            throw new IllegalStateException("Queue already is empty.");
        } else {
            E toReturn = first.value;
            first = first.next;
            return toReturn;
        }
    }

    public boolean isEmpty() {
        return first == null;
    }

    class Node {
        E value;
        Node next;
    }
}
