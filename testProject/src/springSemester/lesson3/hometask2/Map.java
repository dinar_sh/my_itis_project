package springSemester.lesson3.hometask2;

public interface Map<K, V> {
    void put(K key, V value);

    V get(K key);
}
