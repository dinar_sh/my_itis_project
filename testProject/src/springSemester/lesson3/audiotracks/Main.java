package springSemester.lesson3.audiotracks;

import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        AudioTrack a1 = new AudioTrack("abc", "cde", 1);
        AudioTrack a2 = new AudioTrack("фыав", "ads", 10);

        Comparator<AudioTrack> c = new Comparator<AudioTrack>() {
            @Override
            public int compare(AudioTrack o1, AudioTrack o2) {
                return o1.getDuraiton() - o2.getDuraiton();
            }
        };

        Comparator<AudioTrack> c2 = (o1, o2) -> o1.getAuthor().compareTo(o2.getAuthor());
        Playlist p = new Playlist(c);
        p.add(a1);
        p.add(a2);

    }

}
