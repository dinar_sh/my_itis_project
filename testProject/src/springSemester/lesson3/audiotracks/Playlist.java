package springSemester.lesson3.audiotracks;

import java.util.Comparator;

public class Playlist {
    private static final int SIZE = 10;
    private AudioTrack[] trackslist;
    private int size;
    private Comparator<AudioTrack> comparator;

    public Playlist(Comparator<AudioTrack> comparator) {
        this();
        this.comparator = comparator;
    }

    public Playlist() {
        this.trackslist = new AudioTrack[SIZE];
        this.size = 0;
    }

    public void add(AudioTrack track) {
        int i = 0;
        while (i < size) {
            if (comparator == null) {
                if (comparator.compare(track, trackslist[i]) < 0) {
                    break;
                }
            } else {
                if (track.compareTo(trackslist[i]) < 0) {
                    break;
                }
            }
            i++;
        }
        if (size == trackslist.length - 1) {
            AudioTrack[] newlist = new AudioTrack[trackslist.length + trackslist.length >> 1];
            for (int j = 0; j < size; j++) {
                newlist[j] = trackslist[j];
            }
            for (int j = size; j > i; j--) {
                newlist[j] = newlist[j - 1];
            }
            newlist[i] = track;
            trackslist = newlist;
        } else {
            for (int j = size; j > i; j--) {
                trackslist[j] = trackslist[j - 1];
            }
            trackslist[i] = track;
        }
        size++;
    }
}
