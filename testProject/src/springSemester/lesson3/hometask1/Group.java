package springSemester.lesson3.hometask1;

import java.util.Comparator;

public class Group {
    private static final int SIZE = 25;
    private Student[] students;
    private int n;
    private Comparator<Student> comparator;

    public Group() {
        this.students = new Student[SIZE];
        this.n = 0;
    }

    public Group(Comparator<Student> comparator) {
        this();
        this.comparator = comparator;
    }

    public void add(Student student) {
        if (n == students.length) {
            System.out.println("Group is full.");
            return;
        }
        if (comparator != null) {
            int i = 0;
            while (i < n) {
                if (comparator.compare(student, students[i]) < 0) {
                    break;
                }
                i++;
            }
            for (int j = n - 1; j >= i; j--) {
                students[j + 1] = students[j];
            }
            students[i] = student;
            n++;
        } else {
            students[n++] = student;
        }
    }
}
