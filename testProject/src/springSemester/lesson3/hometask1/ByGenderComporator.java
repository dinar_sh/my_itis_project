package springSemester.lesson3.hometask1;

import java.util.Comparator;

public class ByGenderComporator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        if (o1.isMale() == o2.isMale()) {
            return 0;
        }
        if (o1.isMale() == true) {
            return 1;
        } else {
            return -1;
        }
    }
}
