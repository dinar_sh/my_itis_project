package springSemester.lesson8.dao;

import springSemester.lesson8.generators.IdGenerator;
import springSemester.lesson8.generators.UserIdGeneratorImpl;
import springSemester.lesson8.models.User;

import java.io.*;

public class UserDaoTextFileImpl implements UserDao {

    private String fileName;
    private IdGenerator generator;

    public UserDaoTextFileImpl() {
        this.generator = new UserIdGeneratorImpl();
        this.fileName = "users_data.txt";
    }

    @Override
    public User save(User model) {
        try {
            File f = new File(fileName);
            OutputStream os = new FileOutputStream(f, true);
            PrintWriter pw = new PrintWriter(os);
            Long id = generator.getNextId();
            pw.println(id + " " + model.getNickname() + " " + model.getPassword());
            model.setId(id);
            pw.close();
            return model;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("File with name = " + fileName + " not found");
        }
    }

    @Override
    public User find(Long id) {
        return null;
    }

    @Override
    public void update(User model) {

    }

    @Override
    public void delete(Long id) {

    }
}
