package springSemester.lesson8.services;

import springSemester.lesson8.models.User;

public interface UsersService {
    User signUp(User user);

    void singIn(User user);

}
