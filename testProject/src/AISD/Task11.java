package AISD;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;
//http://codeforces.com/problemset/problem/363/B
public class Task11 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int amount = in.nextInt();
        int zabori = in.nextInt();
        int minIndex = 0;
        int minSum = 0;

        Deque<Integer> deque = new LinkedList<>();

        for (int i = 0; i < zabori; i++) {
            deque.addLast(in.nextInt());
            minSum += deque.peekLast();
        }
        int var = minSum;
        for (int i = zabori; i < amount; i++) {
            int s = in.nextInt();
            var = var+ s - deque.removeFirst();
            if (var <= minSum) {
                minIndex = i - zabori+1;
                minSum = var;
            }
            deque.addLast(s);
        }
        System.out.println(minIndex+1);
    }
}
