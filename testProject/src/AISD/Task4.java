package AISD;

import java.io.*;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Task4 {
    public static void main(String[] args) throws IOException {
        File f = new File("input.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter("output.txt"));
        Scanner sc = new Scanner(f);

        int[] array = new int[26];
        Set<Integer> set = new TreeSet<>();

        array[0] = 0;
        array[1] = 1;
        set.add(0);
        for (int i = 2; i < 26; i++) {
            array[i] = array[i - 1] + array[i - 2];
            set.add(array[i]);
        }


        char[] arr = sc.nextLine().toCharArray();
        for (int i = 0; i < arr.length; i++) {
            if(set.contains(i+1)) bw.write((char)arr[i]);
        }

        bw.close();

    }

}
