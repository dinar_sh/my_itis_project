package AISD;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

//http://codeforces.com/problemset/problem/368/B
public class Task12 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt() - 1;
        Set<Integer> set = new TreeSet<>();
        int[] uniq = new int[n];
        uniq[0] = 1;
        int prev = in.nextInt();
        for (int i = 1; i < n; i++) {
            uniq[i] = uniq[i - 1];
            int next = in.nextInt();
            set.add(next);
            if (next != prev) uniq[i]++;
            prev = next;
        }
        for (int i = 0; i <= m; i++) {
            int l = in.nextInt() - 1;
            int var = uniq[n-1] - uniq[l]+1;
            if(var>=set.size()){
                System.out.println(set.size());
            } else{
                System.out.println(var);
            }
        }
        /*Set<Integer> set = new TreeSet<>();
        for (int j = 0; j < m; j++) {
            for (int i = in.nextInt() - 1; i < m; i++) {
                set.add(arr[i]);
            }
            System.out.println(set.size());
            set.clear();
        }*/
    }
}
