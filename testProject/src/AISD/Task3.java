package AISD;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) throws IOException {
        File f = new File("input.txt");
        Scanner sc = new Scanner(f);
        PrintWriter pw = new PrintWriter("output.txt");
        int number = 0;
        if (sc.hasNext()) number = sc.nextInt();
        if (number <= 0) {
            pw.print(0);
            pw.close();
            return;
        }
        int a = 0, b = 1;
        int i = 1;
        while (i != number) {
            b = a + b;
            a = b - a;
            i++;
        }
        pw.print(b % 10);

        sc.close();
        pw.close();
    }
}
