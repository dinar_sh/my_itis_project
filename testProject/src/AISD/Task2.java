package AISD;

import java.io.*;
import java.util.Scanner;

public class Task2 extends Thread {
    public static void main(String[] args) throws IOException {
        File f = new File("input.txt");
        Scanner sc = new Scanner(f);
        PrintWriter pw = new PrintWriter("output.txt");
        int number = 0;
        if (sc.hasNext())
             number = sc.nextInt();

        int a = 0, b = 1;
        int i = 1;
        while (b < number) {
            b = a + b;
            a = b - a;
            i++;
        }

        if(b == number){
            pw.println(1);
            pw.print(i);
        } else{
            pw.print(0);

        }
        sc.close();
        pw.close();
    }
}
