package AISD;

import java.math.BigInteger;
import java.util.Scanner;

public class Task0 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        BigInteger n = BigInteger.valueOf(in.nextLong()); //банки мёда
        BigInteger m = BigInteger.valueOf(in.nextLong()); //коробки
        if (n.compareTo(m) > 0) {
            System.out.println(0);
        } else if (n.compareTo(m) == 0) {
            System.out.println(1);
        } else {
            if ( n.compareTo(BigInteger.valueOf(m.intValue() - n.intValue())) < 0) {

                System.out.println(fact(m, BigInteger.valueOf(m.intValue() - n.intValue() + 1)).divide(fact(n, BigInteger.valueOf(1))).mod(BigInteger.valueOf(1_000_000_009)));
            } else {
                System.out.println(fact(m, BigInteger.valueOf(n.intValue()+1)).divide(fact(BigInteger.valueOf(m.intValue() - n.intValue()), BigInteger.valueOf(1))).mod(BigInteger.valueOf(1_000_000_009)));
            }

        }
    }




    public static BigInteger fact(BigInteger number, BigInteger end) {
        if (number.compareTo(end) == 0) {
            return number;
        }
        return number.multiply(fact(BigInteger.valueOf(number.intValue() - 1), end));

    }
}
