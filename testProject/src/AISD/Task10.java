package AISD;


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

//http://codeforces.com/problemset/problem/313/B
public class Task10 {
    /*public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        BufferedReader bf = new BufferedReader(new InputStreamReader(new DataInputStream(System.in)));
        char[] arr = bf.readLine().toCharArray();

        int query = Integer.valueOf(bf.readLine());
        for (int i = 0; i < query; i++) {
            int first = scanner.nextInt() - 1;
            int second = scanner.nextInt() - 1;
            int count = 0;
            while (first != arr.length - 1 && first < second) {
                if (arr[first] == arr[++first]) count++;
            }
            System.out.println(count);
        }
    }*/

/*    public static void main(String[] args)  {
        Scanner scanner = new Scanner(System.in);
        char[] arr = scanner.nextLine().toCharArray();
        int query = scanner.nextInt();
        for (int i = 0; i < query; i++) {
            int first = scanner.nextInt() - 1;
            int second = scanner.nextInt() - 1;
            int count = 0;
            while (first != arr.length - 1 && first < second) {
                if (arr[first] == arr[++first]) count++;
            }
            System.out.println(count);
        }
    }*/
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        String s = scan.next();
        int n = scan.nextInt();
        int[] a = new int[s.length()];
        for (int i = 1; i < s.length(); i++) {
            a[i]=a[i-1];
            if(s.charAt(i)==s.charAt(i-1)){
                a[i]++;
            }
        }
        for(int i = 0;i<n;i++){
            System.out.println(-1*a[scan.nextInt()-1]+a[scan.nextInt()-1]);
        }
    }

}
