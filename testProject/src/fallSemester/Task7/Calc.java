package fallSemester.Task7;

public class Calc {
    public static void main(String[] args) {
        System.out.println("Result: " + calculation(args));
    }

    public static int calculation(String[] a) {
        switch (a[1]) {
            case "+":
                return (Integer.parseInt(a[0]) + Integer.parseInt(a[2]));
            case "-":
                return (Integer.parseInt(a[0]) - Integer.parseInt(a[2]));
            case "*":
                return (Integer.parseInt(a[0]) * Integer.parseInt(a[2]));
            case "/":
                return (Integer.parseInt(a[0]) / Integer.parseInt(a[2]));
            default:
                return -1;
        }

    }
}
