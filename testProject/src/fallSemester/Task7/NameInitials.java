package fallSemester.Task7;

public class NameInitials {
    public static void main(String[] args) {
        System.out.println(firstLetters(args));
    }

    public static String firstLetters(String[] a) {
        String result = "";
        for (int i = 0; i < a.length; i++) {
            result = result + a[i].charAt(0) + ". ";
        }
        return result;
    }
}
