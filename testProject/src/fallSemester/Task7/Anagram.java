package fallSemester.Task7;

public class Anagram {
    public static void main(String[] args) {
        System.out.println(doAnagram(args));
    }

    public static boolean doAnagram(String[] a) {
        String str1 = a[0];
        String str2 = a[1];
        if (str1.length() != str2.length()) {
            return false;
        } else {
            boolean tag = false;

            for (int i = 0; i < str1.length(); i++) {               //сравниваем первое слово со вторым, в случаее несовпадения - возвращает false

                for (int j = 0; j < str2.length(); j++) {
                    if (str1.charAt(i) == str2.charAt(j)) {
                        tag = true;
                        break;
                    }
                }

                if (tag != true) {
                    return false;
                }
                tag = false;
            }

            for (int i = 0; i < str2.length(); i++) {           //сравниваем второе слово с первым, в случаее несовпадения - возвращает false

                for (int j = 0; j < str1.length(); j++) {
                    if (str2.charAt(i) == str1.charAt(j)) {
                        tag = true;
                        break;
                    }
                }

                if (tag != true) {
                    return false;
                }
                tag = false;
            }

            return true;
        }
    }
}
