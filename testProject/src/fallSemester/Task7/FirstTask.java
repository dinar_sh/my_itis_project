package fallSemester.Task7;

public class FirstTask {
    public static void main(String[] args) {
        System.out.println("Max of Byte: " + Byte.MAX_VALUE);
        System.out.println("Min of Byte: " + Byte.MIN_VALUE);
        System.out.println("Max of Integer: " + Integer.MAX_VALUE);
        System.out.println("Min of Integer: " + Integer.MIN_VALUE);
        System.out.println("Max of Short: " + Short.MAX_VALUE);
        System.out.println("Min of Short: " + Short.MIN_VALUE);
        System.out.println("Max of Double: " + Double.MAX_VALUE);
        System.out.println("Min of Double: " + Double.MIN_VALUE);
        System.out.println("Max of Float: " + Float.MAX_VALUE);
        System.out.println("Min of Float: " + Float.MIN_VALUE);
        System.out.println("Max of Long: " + Long.MAX_VALUE);
        System.out.println("Min of Long: " + Long.MIN_VALUE);

    }
}