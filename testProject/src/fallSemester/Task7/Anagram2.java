package fallSemester.Task7;

import java.util.Arrays;

public class Anagram2 {
    public static void main(String[] args) {
        if (args.length != 2) return;
        if (args[0].length() != args[1].length()) {
            System.out.println(false);
            return;
        }
        char[] word1 = args[0].toCharArray();
        char[] word2 = args[1].toCharArray();

        Arrays.sort(word1);
        Arrays.sort(word2);

        for (int i = 0; i < word1.length; i++) {
            if (word1[i] != word2[i]) {
                System.out.println(false);
                return;
            }
        }
        System.out.println(true);
    }
}
