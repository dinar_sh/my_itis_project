package fallSemester.task11;

import java.util.Scanner;

public class Game2 {
    private static String[][] table = new String[3][3];
    private static String x = "x";
    private static String o = "o";
    private static String winner = "";
    private static int numberOfTurns = 0;
    private static Scanner in = new Scanner(System.in);
    private static String isOver = "";

    public static void main(String[] args) {
        startGame();

    }

    public static void startGame() {
        System.out.println("Это обычные Крестики-Нолики. Чтобы играть - необходимо по очереди вводить координаты (пример формата ввода: 1a или 3c)");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = " ";
            }
        }

        String turn = "";
        int firstIndex;
        int secondIndex;
        displayTable();
        while (isOver.equals("")) {
            numberOfTurns++;
            System.out.print("> Player1's turn:");
            turn = in.nextLine();

            while (true) {
                while (true) {
                    firstIndex = getFirstIndex(turn.substring(0, 1));
                    secondIndex = getSecondIndex(turn.substring(1));
                    if (firstIndex == -1 || secondIndex == -1) {
                        System.out.println("Wrong format!");
                        turn = in.nextLine();
                        continue;
                    } else {
                        break;
                    }
                }
                if (table[firstIndex][secondIndex].equals(" ")) {
                    table[firstIndex][secondIndex] = x;
                    break;
                } else {
                    System.out.println("Клетка уже занята. Введите координаты заново.");
                    turn = in.nextLine();
                }
            }
            displayTable();
            isOver = isGameOver();

            if (!isOver.equals("")) {
                break;
            }


            System.out.print("> Player2's turn:");
            turn = in.nextLine();
            numberOfTurns++;
            while (true) {
                while (true) {
                    firstIndex = getFirstIndex(turn.substring(0, 1));
                    secondIndex = getSecondIndex(turn.substring(1));
                    if (firstIndex == -1 || secondIndex == -1) {
                        System.out.println("Wrong format!");
                        turn = in.nextLine();
                        continue;
                    } else {
                        break;
                    }
                }
                if (table[firstIndex][secondIndex].equals(" ")) {
                    table[firstIndex][secondIndex] = o;
                    break;
                } else {
                    System.out.println("Клетка уже занята. Введите координаты заново.");
                    turn = in.nextLine();
                }
            }
            displayTable();
            isOver = isGameOver();
        }
        System.out.println("\n");
        System.out.println("Result: "+ isOver);
        displayTable();

    }

    private static void displayTable() {
        System.out.println("  | A | B | C |");
        System.out.println("---------------");
        for (int i = 0; i < 3; i++) {
            System.out.print((i + 1) + " | ");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println();
            System.out.println("---------------");
        }
    }

    private static String isGameOver() {
        for (int j = 0; j < 3; j++) {
            if (table[j][0].equals(x) && table[j][1].equals(x) && table[j][2].equals(x)) {      //горизонтальная проверка
                table[j][0] = table[j][0].toUpperCase();
                table[j][1] = table[j][1].toUpperCase();
                table[j][2] = table[j][2].toUpperCase();
                return "Player 1 WIN";
            }
            if (table[0][j].equals(x) && table[1][j].equals(x) && table[2][j].equals(x)) {      //вертикальная проверка
                table[0][j] = table[0][j].toUpperCase();
                table[1][j] = table[1][j].toUpperCase();
                table[2][j] = table[2][j].toUpperCase();
                return "Player 1 WIN";
            }
        }
        if (table[0][0].equals(x) && table[1][1].equals(x) && table[2][2].equals(x)) {          //диагональная проверка
            table[0][0] = table[0][0].toUpperCase();
            table[1][1] = table[1][1].toUpperCase();
            table[2][2] = table[2][2].toUpperCase();
            return "Player 1 WIN";
        }
        if (table[0][2].equals(x) && table[1][1].equals(x) && table[2][0].equals(x)) {          //диагональная проверка
            table[0][2] = table[0][2].toUpperCase();
            table[1][1] = table[1][1].toUpperCase();
            table[2][0] = table[2][0].toUpperCase();
            return "Player 1 WIN";
        }


        for (int j = 0; j < 3; j++) {
            if (table[j][0].equals(o) && table[j][1].equals(o) && table[j][2].equals(o)) {
                table[j][0] = table[j][0].toUpperCase();
                table[j][1] = table[j][1].toUpperCase();
                table[j][2] = table[j][2].toUpperCase();
                return "Player 2 WIN";
            }
            if (table[0][j].equals(o) && table[1][j].equals(o) && table[2][j].equals(o)) {
                table[0][j] = table[0][j].toUpperCase();
                table[1][j] = table[1][j].toUpperCase();
                table[2][j] = table[2][j].toUpperCase();
                return "Player 2 WIN";
            }
        }
        if (table[0][0].equals(o) && table[1][1].equals(o) && table[2][2].equals(o)) {
            table[0][0] = table[0][0].toUpperCase();
            table[1][1] = table[1][1].toUpperCase();
            table[2][2] = table[2][2].toUpperCase();
            return "Player 2 WIN";
        }
        if (table[0][2].equals(o) && table[1][1].equals(o) && table[2][0].equals(o)) {
            table[0][2] = table[0][2].toUpperCase();
            table[1][1] = table[1][1].toUpperCase();
            table[2][0] = table[2][0].toUpperCase();
            return "Player 2 WIN";
        }

        if (numberOfTurns == 9) return "Ничья";

        return "";
    }

    private static int getFirstIndex(String str) {
        switch (str) {
            case "1":
                return 0;
            case "2":
                return 1;
            case "3":
                return 2;
            default:
                return -1;
        }
    }

    private static int getSecondIndex(String str) {
        switch (str.toUpperCase()) {
            case "A":
                return 0;
            case "B":
                return 1;
            case "C":
                return 2;
            default:
                return -1;
        }
    }
}