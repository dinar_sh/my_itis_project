package fallSemester.map;

public class MapCoords {
    private static boolean isXnumber;
    private static boolean isYnumber;
    private static boolean xCharIs;
    private static boolean yCharIs;

    /**
     * this method converts user's input data in real coordinates
     * there are 4 cases:
     * 1) horizontal coordinates - numbers, vertical coordinates - numbers;
     * 2) horizontal coordinates - numbers, vertical coordinates - letters;
     * 3) horizontal coordinates - letters, vertical coordinates - numbers;
     * 4) horizontal coordinates - letters, vertical coordinates - letters.
     *
     * @param str input data from user (coordinates)
     * @return real coordinates if input data correct
     */
    public static int[] getCoords(String str) {
        if (isXnumber) {
            if (isYnumber) {
                try {
                    String coordX = str.substring(0, str.indexOf(' '));
                } catch (Exception e) {
                    return new int[]{-1, -1};
                }

                String coordX = str.substring(0, str.indexOf(' '));
                String coordY = str.substring(str.indexOf(' ') + 1);

                try {
                    int x = Integer.parseInt(coordX);
                    int y = Integer.parseInt(coordY);
                } catch (Exception e) {
                    return new int[]{-1, -1};
                }

                int x = Integer.parseInt(coordX);
                int y = Integer.parseInt(coordY);

                x--;
                y--;

                if (x < 0 || y < 0) {
                    return new int[]{-1, -1};
                }
                return new int[]{x, y};
            } else {
                try {
                    String coordX = str.substring(0, str.indexOf(' '));
                } catch (Exception e) {
                    return new int[]{-1, -1};
                }

                String coordX = str.substring(0, str.indexOf(' '));
                String coordY = str.substring(str.indexOf(' ') + 1);

                try {
                    int x = Integer.parseInt(coordX);
                    int y = ((int) (coordY.toUpperCase().toCharArray()[0])) - 64;
                } catch (Exception e) {
                    return new int[]{-1, -1};
                }

                int x = Integer.parseInt(coordX);
                int y = ((int) (coordY.toUpperCase().toCharArray()[0])) - 64;

                x--;
                y--;

                if (x < 0 || y < 0) {
                    return new int[]{-1, -1};
                }
                return new int[]{x, y};
            }
        } else {
            if (isYnumber) {
                try {
                    String coordX = str.substring(0, str.indexOf(' '));
                } catch (Exception e) {
                    return new int[]{-1, -1};
                }

                String coordX = str.substring(0, str.indexOf(' '));
                String coordY = str.substring(str.indexOf(' ') + 1);

                try {
                    int x = ((int) (coordX.toUpperCase().toCharArray()[0])) - 64;
                    int y = Integer.parseInt(coordY);
                } catch (Exception e) {
                    return new int[]{-1, -1};
                }

                int x = ((int) (coordX.toUpperCase().toCharArray()[0])) - 64;
                int y = Integer.parseInt(coordY);

                x--;
                y--;

                if (x < 0 || y < 0) {
                    return new int[]{-1, -1};
                }
                return new int[]{x, y};
            } else {
                try {
                    String coordX = str.substring(0, str.indexOf(' '));
                } catch (Exception e) {
                    return new int[]{-1, -1};
                }

                String coordX = str.substring(0, str.indexOf(' '));
                String coordY = str.substring(str.indexOf(' ') + 1);

                try {
                    int x = ((int) (coordX.toUpperCase().toCharArray()[0])) - 64;
                    int y = ((int) (coordY.toUpperCase().toCharArray()[0])) - 64;
                } catch (Exception e) {
                    return new int[]{-1, -1};
                }

                int x = ((int) (coordX.toUpperCase().toCharArray()[0])) - 64;
                int y = ((int) (coordY.toUpperCase().toCharArray()[0])) - 64;

                x--;
                y--;

                if (x < 0 || y < 0) {
                    return new int[]{-1, -1};
                }
                return new int[]{x, y};
            }
        }
    }

    /**
     * method uses for choosing coordinate system
     *
     * @param one
     * @param two
     */
    public static void chooseCoords(int one, int two) {
        switch (one) {
            case 1:
                isXnumber = true;
                break;
            case 2:
                xCharIs = true;
                break;
        }

        switch (two) {
            case 1:
                isYnumber = true;
                break;
            case 2:
                yCharIs = true;
                break;
        }
    }

    /**
     * this method give information for drawing fallSemester.map in console
     *
     * @return information about coordinate system
     * @see MapDisplayConsole
     */
    public static boolean[] returnArray() {
        return new boolean[]{isXnumber, isYnumber, xCharIs, yCharIs};
    }

}
