package fallSemester.map;

import java.util.Scanner;

public class DemoMap {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        MapArea map = new MapArea(15, 15);

        System.out.println("Выбери ситему координат:");
        System.out.println("                         По горизонтали ");
        System.out.println("                                         1) Цифры");
        System.out.println("                                         2) Буквы");

        int x, y;

        while (true) {
            x = in.nextInt();
            if (x == 1 || x == 2) {
                break;
            }
            System.out.println("Неверный формат.");
        }
        System.out.println("                         По вертикали ");
        System.out.println("                                         1) Цифры");
        System.out.println("                                         2) Буквы");

        while (true) {
            y = in.nextInt();
            if (y == 1 || y == 2) {
                break;
            }
            System.out.println("Неверный формат.");
        }
        MapCoords.chooseCoords(x, y);
        MapDisplayConsole.print(15, 15, map.getArea());

        map.addElement("B B", '♦');                     //конкретный пример ввода (по горизонт. - буквы, по вертикал. - цифры)
        MapDisplayConsole.print(15, 15, map.getArea());
    }
}
