package fallSemester.Task4;

public class Alarm {
    private int timeAlarm;

    public Alarm(int timeAlarm) {
        setTimeAlarm(timeAlarm);
    }

    public int getTimeAlarm() {
        return timeAlarm;
    }

    public void alarmNow() {
        System.out.println("ALARM: BRRRRRRRRRNG BRRRRRRRRRRRRRNG");
    }

    public void setTimeAlarm(int i) {
        timeAlarm = i;
    }
}
