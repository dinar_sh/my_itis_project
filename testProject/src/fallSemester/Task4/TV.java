package fallSemester.Task4;

public class TV {
    private Screen myScreen;
    private int volume;

    public TV() {
        myScreen = new Screen();
        volume = 30;
    }

    public void showChannelOnScreen(int numberOfChannel) {
        myScreen.showChannel(numberOfChannel);
    }

    public void changeVolumeUp() {
        volume++;
        if (volume > 100) {
            volume = 100;
            myScreen.showVolume(volume, false);
        } else{
            myScreen.showVolume(volume, true);
        }
    }

    public void changeVolumeDown() {
        volume--;
        if (volume < 0) {
            volume = 0;
            myScreen.showVolume(volume, false);
        } else {
            myScreen.showVolume(volume,true);
        }
    }
}
