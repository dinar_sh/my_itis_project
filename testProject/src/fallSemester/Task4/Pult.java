package fallSemester.Task4;

public class Pult {
    private TvBox myTvBox;
    private boolean isTurnOn;

    public Pult() {
        myTvBox = new TvBox();
        isTurnOn = false;
    }

    public void turnOnTV() {
        if (!isTurnOn) {
            isTurnOn = true;
            showChannel();
        } else {
            System.out.println("TV is ON already");
        }
    }

    public void turnOffTV() {
        if (isTurnOn) {
            isTurnOn = false;
            System.out.println("TV is OFF");
        } else {
            System.out.println("TV is OFF already");
        }
    }

    public void changeChannelUp() {
        if (isTurnOn) {
            myTvBox.channelUp();
        } else {
            System.out.println("Impossible change Channel - TV is OFF");
        }
    }

    public void changeChannelDown() {
        if (isTurnOn) {
            myTvBox.channelDown();
        } else {
            System.out.println("Impossible change Channel - TV is OFF");
        }
    }

    public void changeVolumeUp() {
        if (isTurnOn) {
            myTvBox.volumeUp();
        } else {
            System.out.println("Impossible change volume - TV is OFF");
        }
    }

    public void changeVolumeDown() {
        if (isTurnOn) {
            myTvBox.volumeDown();
        } else {
            System.out.println("Impossible change volume - TV is OFF");
        }
    }

    private void showChannel() {     //используется только тогда, когда включается TV (т.е. при включении ТВ показывает текущий канал)
        myTvBox.showChannel();
    }

}
