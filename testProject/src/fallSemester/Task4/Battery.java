package fallSemester.Task4;

public class Battery {

    private int capacity = 10;

    public Battery() {
    }

    public int decrease() {
        return --capacity;
    }

    public int charge(int i) {
        return capacity += i;
    }

    public int getCapacity(){
        return capacity;
    }

}
