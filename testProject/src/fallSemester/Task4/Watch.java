package fallSemester.Task4;

public class Watch {
    private Battery watchBattery;
    private boolean tag = true;

    public Watch() {
        watchBattery = new Battery();
    }

    public void tick() {
        if (watchBattery.getCapacity() > 0) {
            if (tag) {
                watchBattery.decrease();
                System.out.println("Тик ");
                tag = false;
            } else {
                watchBattery.decrease();
                System.out.println("Так");
                tag = true;
            }
        } else {
            System.out.println("Battery died. Needs charge.");
        }
    }

    public int chargeWatch() {
        return watchBattery.charge(10);
    }
}
