package fallSemester.Lessson_2_1;

public class MainProgramm {
    public static void main(String[] args) {
        Point originOne = new Point(23, 94);
        Rectangle1 rectOne = new Rectangle1(originOne, 100, 200);
        Rectangle1 rectTwo = new Rectangle1(50, 100);

        rectTwo.origin = originOne;

        rectOne.move(40, 72);
        System.out.println(rectTwo.origin.x);
        System.out.println(rectTwo.origin.y);
    }
}
