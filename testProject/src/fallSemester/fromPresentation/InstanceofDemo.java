package fallSemester.fromPresentation;

public class InstanceofDemo {
    public static void main(String[] args) {

        Parent obj1 = new Parent();
        Parent obj2 = new Child();

        System.out.println("obj1 istanceof Parent: " + (obj1 instanceof Parent));
        System.out.println("obj1 istanceof Child: " + (obj1 instanceof Child));
        System.out.println("obj1 istanceof MyInterface: " + (obj1 instanceof MyInterface));
        System.out.println("obj2 istanceof Parent: " + (obj2 instanceof Parent));
        System.out.println("obj2 istanceof Child: " + (obj2 instanceof Child));
        System.out.println("obj2 istanceof MyInterface: " + (obj2 instanceof MyInterface));

    }
}

class Parent {
}

class Child extends Parent implements MyInterface {
}

interface MyInterface {
}
