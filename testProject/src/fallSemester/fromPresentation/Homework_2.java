package fallSemester.fromPresentation;

public class Homework_2 {
    public static void main(String[] args) {
        // Finding number of vowels in the string

        String anyString = "aeiouyAEIOUYqwrt";
        int numberOfVowels = 0;
        char chr;
        for (int i = 0; i < anyString.length(); i++) {
            chr = anyString.charAt(i);
            if ((chr == 'a') || (chr == 'e') || (chr == 'i') || (chr == 'o') || (chr == 'u') || (chr == 'y') || (chr == 'A') || (chr == 'E') || (chr == 'I') || (chr == 'O') || (chr == 'U') || (chr == 'Y')) {
                numberOfVowels++;
            }
        }
        System.out.println("Number of vowels in the string is: " + numberOfVowels);
    }
}
