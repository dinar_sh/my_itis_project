package fallSemester.Task5;

public class HomeTask1 {
    public static void main(String[] args) {
        String str = "Information Technology";
        String strResult = "";

        for (int i = 0; i < str.length(); i++) {
            if (i % 2 != 0) {
                strResult += str.substring(i, i + 1).toUpperCase();
            } else {
                strResult += str.substring(i, i + 1).toLowerCase();
            }
        }

        System.out.println(strResult);
    }
}
