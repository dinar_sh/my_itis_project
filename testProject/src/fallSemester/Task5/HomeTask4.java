package fallSemester.Task5;

public class HomeTask4 {
    public static void main(String[] args) {
        String str = "AAAAA bbbbbb";

        System.out.println(str.replace('b', 'A'));
        System.out.println(str.replaceAll("AAAA", "FFFF"));
        System.out.println(str.substring(0, 7));
    }
}
