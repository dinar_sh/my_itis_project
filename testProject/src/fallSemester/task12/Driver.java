package fallSemester.task12;

public class Driver {
    public void drive(Drivable drivable, int distance) {
        drivable.setDistance(distance);
    }
}
