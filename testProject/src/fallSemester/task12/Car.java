package fallSemester.task12;

public class Car implements Fuelable, Drivable {
    private int distance;
    private int fuel = 0;

    public int getFuel() {
        return fuel;
    }

    @Override
    public void sefFuel(int i) {
        fuel = i;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance += distance;
    }
}
