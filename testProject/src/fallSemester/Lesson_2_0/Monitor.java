package fallSemester.Lesson_2_0;

public class Monitor {
    private boolean isOn;
    private double diagonal; // ins.
    private String brandName;

    public Monitor() {
        isOn = false;
        diagonal = 0;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setDiagonal(double diagonal) {
        this.diagonal = diagonal;
    }

    public double getDiagonal() {
        return diagonal;
    }

    public void turnOn() {
        isOn = true;
    }

    public void turnOff() {
        isOn = false;
    }

    public boolean isOn() {
        return isOn;
    }
}
