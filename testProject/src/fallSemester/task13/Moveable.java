package fallSemester.task13;

public interface Moveable {
    void moveTo(int x, int y);
}
