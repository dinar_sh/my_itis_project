package fallSemester.task13;

public class Point implements Moveable, Placeable {

    Coords coords;

    public Point() {
        coords = new Coords();
    }


    @Override
    public void moveTo(int x, int y) {
        coords.setX(x);
        coords.setY(y);
    }

    @Override
    public void placeTo(Coords coords) {
        this.coords = coords;
    }

    @Override
    public Coords getPlace() {
        return coords;
    }
}
