package fallSemester.classwork_wtihHomeWork;

public class Rectangle {
    private int width;
    private int length;

    public Rectangle(int width, int length) {
        this.length = length;
        this.width = width;
    }

    public void display(char chr) {
        if (width <= 0 || length <= 0) {
            System.out.println("rectangle size error");
        } else {
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < length; j++) {
                    System.out.print(chr + " ");
                }
                System.out.println();
            }
        }
    }
}
