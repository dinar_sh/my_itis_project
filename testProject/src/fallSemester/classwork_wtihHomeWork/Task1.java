package fallSemester.classwork_wtihHomeWork;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int row = in.nextInt();
        printSmthng(row);
        System.out.println();
        printSmthng_mirror_right(row);
        System.out.println();
        printSmthng_mirror_down(row);
    }

    public static void printSmthng(int rowCount) {
        String str1 = "*";
        String str2 = " ";

        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(str1 + str2);
            }
            System.out.println();
        }
    }

    public static void printSmthng_mirror_right(int rowCount) {
        String str1 = "*";
        String str2 = " ";
        int a = 0;

        for (int i = 0; i < rowCount; i++) {
            a = rowCount * 2 - (i + 1) * 2;
            for (int m = 0; m < a; m++) {
                System.out.print(str2);
            }
            for (int n = 0; n <= i; n++) {
                System.out.print(str1 + str2);
            }
            System.out.println();
        }
    }

    public static void printSmthng_mirror_down(int rowCount) {
        String str1 = "*";
        String str2 = " ";

        for (int i = rowCount; i >0; i--) {
            for (int j = 1; j <= i; j++) {
                System.out.print(str1 + str2);
            }
            System.out.println();
        }
    }
}
