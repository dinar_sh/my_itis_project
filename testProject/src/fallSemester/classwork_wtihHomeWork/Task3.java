package fallSemester.classwork_wtihHomeWork;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int row = in.nextInt();
        pyramid(row);
    }

    public static void pyramid(int rowCount) {
        String str1 = "*";
        String str2 = " ";
        int a = 0;
        int k = 0;

        for (int i = 0; i < rowCount; i++) {
            a = rowCount - i - 1;

            for (int n = 0; n < a; n++) {
                System.out.print(str2);
            }

            k = 2 * i + 1;
            for (int m = 0; m < k; m++) {
                System.out.print(str1);
            }
            System.out.println();
        }
    }
}
