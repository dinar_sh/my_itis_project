package fallSemester.task9;

public class PerimeterRectangle {

    public void getPerimeter() throws NoLessThanZeroCheckedException {

        Rectangle myRectangle = new Rectangle();
        myRectangle.setLength(5);
        myRectangle.setWidth(-1);

        System.out.println(2 * (myRectangle.getLength() + myRectangle.getWidth()));

    }
}
