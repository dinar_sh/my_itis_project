package fallSemester.task9;

public class NoLessThanZeroCheckedException extends Exception {
    public NoLessThanZeroCheckedException() {
    }

    public NoLessThanZeroCheckedException(String message) {
        super(message);
    }

    public NoLessThanZeroCheckedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoLessThanZeroCheckedException(Throwable cause) {
        super(cause);
    }

    public NoLessThanZeroCheckedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
