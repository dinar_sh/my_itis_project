package fallSemester.task14;

public class Box<E> {
    private E item;

    public void setItem(E item) {
        this.item = item;
    }

    public E getItem() {
        return item;
    }

}
