package fallSemester.tasks;

import java.util.Scanner;

public class Task021 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = 2 * n - 1;
        int d;
        int g;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) {
                System.out.print(" ");
            }
            k--;
            d = 2 * i + 1;
            for (int m = 0; m < d; m++) {
                System.out.print("*");
            }
            System.out.println();
        }

        System.out.println();
        k = 2 * n - 1;
        for (int i = 0; i < n; i++) {
            g = n - i - 1;

            for (int j = 0; j < g; j++) {
                System.out.print(" ");
            }
            d = 2 * i + 1;
            for (int m = 0; m < d; m++) {
                System.out.print("*");
            }
            for (int l = 0; l < k; l++) {
                System.out.print(" ");
            }
            k -= 2;
            for (int m = 0; m < d; m++) {
                System.out.print("*");
            }

            System.out.println();


        }


    }


}
