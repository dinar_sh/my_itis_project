package fallSemester.tasks;

import java.util.Scanner;

public class Task015 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        double x = in.nextDouble();

        System.out.println(task15(1, n, x));
    }


    public static double task15(int i, int n, double x) {
        if (i < n) {
            return (i + x / task15(++i, n, x));
        } else return i + n;
    }
}
