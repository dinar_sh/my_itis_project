package fallSemester.tasks;

import java.util.Scanner;

public class Task014 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        double x = in.nextDouble();
        System.out.println(cosCounting(n,x));
    }

    public static double cosCounting(int n, double x) {
        if (n <= 0) {
            return 0;
        } else {
            return Math.cos(x + cosCounting(--n, x));
        }
    }
}
