package fallSemester.tasks;

import java.util.Scanner;

public class Task012 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double result = 0;
        int n = in.nextInt();
        boolean tag = true;

        for (int i = 1; i < 2 * n; i += 2) {
            if (tag) {
                result = result +  (1.0 / (i * i));
                tag = false;
            } else {
                result = result -  (1.0 / (i * i));
                tag = true;
            }
        }

        System.out.println(result);
    }
}
