package fallSemester.tasks;

public class Task030 {
    public static void main(String[] args) {
        int count = 0;
        int[] ar = new int[args.length];

        int first = 0, second = 0;

        for (int i = 0; i < ar.length; i++) {
            ar[i] = Integer.parseInt(args[i]);
        }

        for (int i = 0; i < ar.length; i++) {
            if (checkNumberOfDigits(ar[i])) {
                if (allDigitsEven(ar[i]) || allDigitsOdd(ar[i])) {
                    count++;
                    if (first == 0) {
                        first = ar[i];
                    } else {
                        second = ar[i];
                    }
                    if (count > 2) {
                        System.out.println("Больше двух чисел");
                        System.exit(0);
                    }
                }
            }
        }
        if (count == 2) {
            System.out.println(first + " " + second);
        }
        else {
            System.out.println("Таких чисел меньше двух");
        }


    }

    private static boolean allDigitsOdd(int x) {
        int lengthX = String.valueOf(x).length();
        int var;
        for (int i = 0; i < lengthX; i++) {
            var = x % degree(10, i + 1) / degree(10, i);
            if (var % 2 == 0) {
                return false;
            }
        }
        return true;
    }

    private static boolean allDigitsEven(int x) {
        int lengthX = String.valueOf(x).length();
        int var;
        for (int i = 0; i < lengthX; i++) {
            var = x % degree(10, i + 1) / degree(10, i);
            if (var % 2 != 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkNumberOfDigits(int x) {
        if ((String.valueOf(x)).length() == 3 || (String.valueOf(x)).length() == 5) {
            return true;
        } else return false;
    }

    public static int degree(int n, int deg) {
        if (deg == 0) return 1;
        return n * degree(n, deg - 1);
    }
}
