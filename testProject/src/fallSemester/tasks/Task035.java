package fallSemester.tasks;

public class Task035 {
    public static void bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = i; j < arr.length; j++) {
                if (arr[i] > arr[j]) {
                    int k = arr[i];
                    arr[i] = arr[j];
                    arr[j] = k;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {12, 33, 4, 456, -9, 0, 2223, -98};
        bubbleSort(arr);
        for(int i = 0; i< arr.length; i++){
            System.out.print(arr[i]+ " ");
        }
    }
}