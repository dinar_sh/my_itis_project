package fallSemester.tasks;

import java.util.Scanner;

public class Task016 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int x = in.nextInt();

        int result = 0;

        for (int i = 1; i <= n; i++) {
            if (i != 1) result = result + result * (x + i);
            else {
                result = 1;
                result = (x + i);
            }
        }
    }
}
