package fallSemester.tasks;

public class Task023 {
    public static void main(String[] args) {
        System.out.println(calcFormula());
    }

    private static double roundDouble(double a) {
        return (Math.round(a * 1_000_000_000) * 1.0 / 1_000_000_000);
    }

    public static double calcFormula() {
        double first = 0;
        double second = 0;
        boolean tag = true;

        for (int i = 1; true; i++) {
            if (tag) {
                first = second + (2.0 * i + 3) / (5.0 * i * i * i * i + 1);
                tag = false;
            } else {
                second = first + (2.0 * i + 3) / (5.0 * i * i * i * i + 1);
                tag = true;
            }
            if (Math.abs(first - second) < 0.000_000_001) {
                if (tag) {
                    return roundDouble(second);
                } else {
                    return roundDouble(first);
                }
            }
        }
    }
}
