package fallSemester.tasks;

public class Task004 {
    public static void main(String[] args) {

        double y = 20;
        double x = 10;


        double result = 0;
        double action1 = 0;
        double action2 = 0;
        double action3 = 0;
        double action4 = 0;
        double action5 = 0;
        double action6 = 0;
        double action7 = 0;
        double action8 = 0;


        action1 = 1 + y;
        action2 = x * x - 4;
        action3 = 1 / action2;
        action4 = y + action3;
        action5 = x + y;
        action6 = action5 / action4;
        action7 = 2 * x + y * y;
        action8 = action7 - action6;
        result = action1 * action8;

        System.out.println(result);
    }
}
