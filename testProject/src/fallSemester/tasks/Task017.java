package fallSemester.tasks;

import java.util.Scanner;

public class Task017 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        double result = 0;

        for (int m = 1; m <= n; m++) {
            result += getFactorialSquare(m - 1) * 1.0 / getFactorial(2 * m);      // хотя можно сделать и по-другому, просто упростив формулу в задании
        }                                                                               // (m - 1)! / (m * (m + 1) * ... * 2*m)
        System.out.println(result);
    }

    public static long getFactorial(int a) {
        if (a == 0) return 1;
        return a * getFactorial(a - 1);
    }

    public static long getFactorialSquare(int a) {
        if (a == 0) return 1;
        return a * a * getFactorial(a - 1);
    }
}
