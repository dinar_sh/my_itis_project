package fallSemester.tasks;

import java.util.Scanner;

public class Task018 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();           //1st variable
        int x = in.nextInt();           //2nd variable
        double result = 0;              //3d variable

        for (int i = 0; i <= n; i++) {  //4th variable
            int a = in.nextInt();       //5th variable
            result = result + a * Math.pow(x, i);
        }

        System.out.println(result);
    }
}
