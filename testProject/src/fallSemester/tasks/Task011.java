package fallSemester.tasks;

import java.util.Scanner;

public class Task011 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int result = 1;

        while (x > 0) {
            result *= x;
            x -= 2;
        }
        System.out.println(result);
    }
}
