package fallSemester.tasks;

public class Task006 {
    public static void main(String[] args) {
        // x^5 + 6*x^4 + 10*x^3 +25*x^2 + 30*x + 101 = 101 + x*(30 + x*(25 + x*(10 + x*(6 + x)))) <- Схема Горнера
        int x = 7;
        int result = 0;
        result = result + (6 + x);
        result *= x;
        result += 10;
        result *= x;
        result += 25;
        result *= x;
        result += 30;
        result *= x;
        result += 101;
        System.out.println(result);

    }
}
