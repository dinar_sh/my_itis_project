package fallSemester.finalExam.task1;

public abstract class HigherEducationInstitution {
    protected String name;

    public abstract void addFaculty(Faculty faculty);
    public abstract void removeaddFaculty(String nameFaculty);

    public String getName() {
        return name;
    }
}
