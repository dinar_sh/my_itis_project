package fallSemester.finalExam.task1;

public class KFU extends HigherEducationInstitution {
    final String name = "KFU";
    private Faculty[] faculties;
    private final int CAPACITY = 100;
    private int size;

    KFU() {
        faculties = new Faculty[CAPACITY];
        size = CAPACITY;
    }

    KFU(Faculty[] faculties) {
        this.faculties = faculties;
        size = faculties.length;
    }

    @Override
    public void addFaculty(Faculty faculty) {
        faculties[size] = faculty;
        size++;
    }

    @Override
    public void removeaddFaculty(String nameFaculty) {
        //удаляем
        size--;
    }
}
