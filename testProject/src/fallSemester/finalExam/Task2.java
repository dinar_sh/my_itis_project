package fallSemester.finalExam;

//x^2 + (2*i +i!)+ a[i] = 0;
public class Task2 {
    public static void main(String[] args) {
        int n = 123456;
        int[] array = new int[n];

        fillArray(array);

        int k = String.valueOf(n).length();      //длина числа размера массива
        int[] arrK = new int[k];                            //сами цифры длины числа размера массива
        fillArrK(arrK, n);
        int sum = summAr(arrK);                             //сумма цифр этого числа

        if (!fillArrK(arrK, sum)) {
            System.out.println(false);
            return;
        }


        double[] solution = new double[2];
        for (int i = 0; i < array.length; i++) {
            if (quadraticEquation(i, array[i], solution)) {  // проверяем есть ли уравнения у этого уравнения (D >= 0?)
                //проверяем целые ли решения уравнения
                //если оба решения(или одно, в случае D=0) целые, то выводит true
                if (isInteger(solution)) {
                    System.out.println(true);
                    return;
                }
            }
        }
    }

    private static boolean isInteger(double[] arr) {
        if (arr[0] == arr[1]) {
            if (arr[0] % 1 == 0) {
                return true;
            }
            return false;
        } else {
            if (arr[0] % 1 == 0 && arr[1] % 1 == 0) {
                return true;
            }
            return false;
        }
    }


    private static boolean quadraticEquation(int i, int ai, double[] solutions) {
        final int a = 1;
        final int b = 2 * i + factorial(i);
        final int c = factorial(ai);
        final int DISCRIMINANT = b * b - 4 * a * c;
        if (DISCRIMINANT < 0) {
            return false;
        }
        final double ROOT_DISCIMINANT = Math.sqrt(DISCRIMINANT);
        if (DISCRIMINANT == 0) {
            solutions[0] = solutions[1] = -1 * b / (2 * a);
        } else {
            solutions[0] = (-1 * b + ROOT_DISCIMINANT) / (2 * a);
            solutions[1] = (-1 * b - ROOT_DISCIMINANT) / (2 * a);
        }
        return true;

    }

    private static int factorial(int i) {
        if (i < 2) {
            return 1;
        }
        return i * factorial(--i);
    }

    private static boolean fillArrK(int[] arrk, int number) {
        boolean even = false;
        int k = arrk.length - 1;
        for (int i = arrk.length - 1; i >= 0; i--) {
            k = k - i;
            arrk[k] = (number / (int) Math.pow(10, i)) % 10;

            if (even || arrk[k] % 2 == 0) even = true;
            k = arrk.length - 1;
        }
        return even;

    }

    private static int summAr(int[] arr) {
        int summ = 0;
        for (int i = 0; i < arr.length; i++) {
            summ += arr[i];
        }
        return summ;
    }

    private static void fillArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = MyRandom.getRandom(1, 100);
        }
    }
}
