package fallSemester.rpg;

public class Artifact {
    private String types[] = {"helmet", "boots", "chestplate", "shield", "weapon"};
    private String type;
    private int hp;
    private int armor;
    private int agility;
    private int critical;
    private int strength;
    private int chanceDropAfterDeath; //no more than 100
    private String name;

    public Artifact(String type, String name, int hp, int armor, int agility, int critical, int strength, int chanceDropAfterDeath) {
        boolean found = false;
        for (String type1 : types) {
            if (type.equals(type1)) {
                found = true;
                this.type = type;
                this.name = name;
                this.hp = hp;
                this.armor = armor;
                this.agility = agility;
                this.critical = critical;
                this.strength = strength;
                this.chanceDropAfterDeath = chanceDropAfterDeath;
            }
        }

        if (!found) {
            throw new RuntimeException("Wrong artifact type " + type);
        }
    }

    public String getType() {
        return type;
    }

    public int getBonusHp() {
        return hp;
    }

    public int getBonusArmor() {
        return armor;
    }

    public int getBonusStrength(){
        return strength;
    }

    public int getBonusAgility() {
        return agility;
    }

    public int getBonusCritical() {
        return critical;
    }

    public int getChanceDropAfterDeath() {
        return chanceDropAfterDeath;
    }

    public String getName(){
        return name;
    }
}
