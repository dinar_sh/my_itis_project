package fallSemester.rpg;

public class Character {
    protected int level;
    protected int hp;              //no more than 100
    protected int currentHp;
    protected int strength;
    private String name;

    protected int critical;        //in % (no more than 100%)
    protected int agility;         //in % (no more than 100%)
    protected int armor;

    protected int antiCritical;    //in % (no more than 100%)
    protected int antiAgility;     //in % (no more than 100%)

    Artifact art[] = new Artifact[5];


    public Character(String name) {
        this.name = name;
        level = 1;
        hp = 100;
        currentHp = hp;
        strength = 10;
        critical = 25;
        agility = 25;
        antiAgility = 0;
        antiCritical = 0;
        armor = 1;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return currentHp;
    }

    public int getLevel() {
        return level;
    }

    public void setCritical(int crit) {
        critical = crit;
    }

    public int getCritical() {
        return critical;
    }

    public void setAgility(int agil) {
        agility = agil;
    }

    public int getAgility() {
        return agility;
    }

    public boolean isDead() {
        return currentHp < 1;
    }

    public boolean isAlive() {
        return currentHp > 0;
    }

    protected int getMaxHit() {
        return strength + level;
    }

    protected int getMinHit() {
        return strength - level;
    }

    protected int hitted(int hit) {
        if (isMiss()) {
            System.out.println("Miss. " + getName() + "[" + getHp() + "] " + " received " + 0 + " damage. " + getName() + "[" + getHp() + "]");
            return 0;
        } else {
            if (hit > getMaxHit()) {
                hit = hit - armor;
                System.out.println("CRIIITICAL DAMAEGE!" + getName() + "[" + getHp() + "] " + " received " + hit + " damage. " + getName() + "[" + (getHp() - hit) + "]");
            } else {
                hit = hit - armor;
                System.out.println(getName() + "[" + getHp() + "] " + " received " + hit + " damage. " + getName() + "[" + (getHp() - hit) + "]");
            }
            return currentHp = currentHp - hit;
        }
    }

    protected int getHit() {
        if (isCriticall()) {
            return 2 * RandomRange.getRandom(getMinHit(), getMaxHit());
        } else {
            return RandomRange.getRandom(getMinHit(), getMaxHit());
        }
    }

    protected boolean isCriticall() {
        if ((RandomRange.getRandom(1, 100) - getAntiCritical()) <= getCritical()) {
            return true;
        } else {
            return false;
        }
    }

    protected boolean isMiss() {
        if ((RandomRange.getRandom(1, 100) - getAntiAgility()) <= getAgility()) {
            return true;
        } else {
            return false;
        }
    }

    public int makeHit(Character character1) {
        return character1.hitted(getHit());
    }

    public void setAntiCritical(int antiCritical) {
        this.antiCritical = antiCritical;
    }

    public void setAntiAgility(int antiAgility) {
        this.antiAgility = antiAgility;
    }

    protected int getAntiAgility() {
        return antiAgility;
    }

    protected int getAntiCritical() {
        return antiCritical;
    }

    public void addArtifact(Artifact artifact) {
        switch (artifact.getType()) {
            case "weapon":
                art[0] = artifact;
                break;
            case "helmet":
                art[1] = artifact;
                break;
            case "chestplate":
                art[2] = artifact;
                break;
            case "boots":
                art[3] = artifact;
                break;
            case "shield":
                art[4] = artifact;
                break;
            default:
                throw new RuntimeException("Wrong artifact type " + art);
        }
        updateStatsAfterAddingArtifact(artifact);
    }

    protected void updateStatsAfterAddingArtifact(Artifact artifact) {
        hp = hp + artifact.getBonusHp();
        currentHp = currentHp + artifact.getBonusHp();
        agility = agility + artifact.getBonusAgility();
        armor = armor + artifact.getBonusArmor();
        critical = critical + artifact.getBonusCritical();
    }

    public void info() {
        System.out.println(getName() + " stats:");
        System.out.println("    Name: " + getName());
        System.out.println("    Level: " + getLevel());
        System.out.println("    " + getHp() + "HP out of " + hp + "HP");
        System.out.println("    Armor: " + armor);
        System.out.println("    Agility: " + getAgility());
        System.out.println("    Strength: " + strength);
        System.out.println("    Critical hit chance: " + getCritical() + "%");

    }

    public String getFirstWord() {  //нужен, чтобы выводилась правильная буква, при выводне Map на консольке
        return "c";
    }
}

//int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);

