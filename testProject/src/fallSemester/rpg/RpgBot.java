package fallSemester.rpg;

public class RpgBot extends Character {
    Artifact botWeapon;

    public RpgBot(String name) {
        super(name);
        botWeapon = new Artifact("weapon", "Excalibur", 0, 0, 0, 3, 2, 50);
    }


    @Override
    protected int hitted(int hit) {
        if (isMiss()) {
            System.out.println("Miss. " + getName() + "[" + getHp() + "] " + " received " + 0 + " damage. " + getName() + "[" + getHp() + "]");
            return 0;
        } else {
            if (hit > getMaxHit()) {
                hit = hit - armor;
                System.out.println("CRIIITICAL DAMAEGE!" + getName() + "[" + getHp() + "] " + " received " + hit + " damage. " + getName() + "[" + (getHp() - hit) + "]");
            } else {
                hit = hit - armor;
                System.out.println(getName() + "[" + getHp() + "] " + " received " + hit + " damage. " + getName() + "[" + (getHp() - hit) + "]");
            }
            if (getHp() - hit < 1) {
                if (chanceDropAfterDeath()) {
                    System.out.println(botWeapon.getType() + " \"Excalibur\"" + " is dropped");
                }
            }
            return currentHp = currentHp - hit;
        }
    }

    private boolean chanceDropAfterDeath() {
        if (RandomRange.getRandom(1, 100) <= botWeapon.getChanceDropAfterDeath()) {
            return true;
        } else
            return false;
    }

    @Override
    public String getFirstWord() {
        return "b";
    }
}
