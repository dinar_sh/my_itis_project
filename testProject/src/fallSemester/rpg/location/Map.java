package fallSemester.rpg.location;

import fallSemester.rpg.BotFightTest;
import fallSemester.rpg.Character;

import java.util.Scanner;

public class Map {
    private Character[][] mapCharacter;
    private int verticalSize;
    private int horizontalSize;
    private Scanner in = new Scanner(System.in);

    public Map(int horizontalSize, int verticalSize) {
        this.verticalSize = verticalSize;
        this.horizontalSize = horizontalSize;
        mapCharacter = new Character[horizontalSize][verticalSize];
    }

    public int getVerticalSize() {
        return verticalSize;
    }

    public int getHorizontalSize() {
        return horizontalSize;
    }

    public void addCharacterAtMap(Character character, int verticalPosition, int horizontalPosition) {
        if (mapCharacter[horizontalPosition - 1][verticalPosition - 1] == null) {
            mapCharacter[horizontalPosition - 1][verticalPosition - 1] = character;
        }
    }

    public Character whoIsThere(int i, int j) {
        return mapCharacter[i][j];
    }

    public void characterMove(Character character, String str) {
        int i = findCharacterHorizontal(character);
        int j = findCharacterVertical(character);
        switch (str) {
            case "w":
                if (i - 1 >= 0) {
                    if (mapCharacter[i - 1][j] != null && mapCharacter[i - 1][j].getFirstWord().equals("b")) {
                        System.out.println("На этой клетке находится противник. Желаете с ним сразиться? (напишите \"yes\" или \"no\")");
                        System.out.print("> ");
                        if (checkingAnswer(in.nextLine())) {
                            if (BotFightTest.fight(character, mapCharacter[i - 1][j])) {
                                mapCharacter[i - 1][j] = mapCharacter[i][j];
                                mapCharacter[i][j] = null;
                            } else {
                                mapCharacter[i][j] = null;
                            }
                        }
                    } else {
                        mapCharacter[i - 1][j] = mapCharacter[i][j];
                        mapCharacter[i][j] = null;
                    }
                }
                break;
            case "a":
                if (j - 1 >= 0) {
                    if (mapCharacter[i][j - 1] != null && mapCharacter[i][j - 1].getFirstWord().equals("b")) {
                        System.out.println("На этой клетке находится противник. Желаете с ним сразиться? (напишите \"yes\" или \"no\")");
                        System.out.print("> ");
                        if (checkingAnswer(in.nextLine())) {
                            if (BotFightTest.fight(character, mapCharacter[i][j - 1])) {
                                mapCharacter[i][j - 1] = mapCharacter[i][j];
                                mapCharacter[i][j] = null;
                            } else {
                                mapCharacter[i][j] = null;
                            }
                        }

                    } else {
                        mapCharacter[i][j - 1] = mapCharacter[i][j];
                        mapCharacter[i][j] = null;
                    }
                }
                break;
            case "s":
                if (i + 1 < horizontalSize) {
                    if (mapCharacter[i + 1][j] != null && mapCharacter[i + 1][j].getFirstWord().equals("b")) {
                        System.out.println("На этой клетке находится противник. Желаете с ним сразиться? (напишите \"yes\" или \"no\")");
                        System.out.print("> ");
                        if (checkingAnswer(in.nextLine())) {
                            if (BotFightTest.fight(character, mapCharacter[i + 1][j])) {
                                mapCharacter[i + 1][j] = mapCharacter[i][j];
                                mapCharacter[i][j] = null;
                            } else {
                                mapCharacter[i][j] = null;
                            }
                        }
                    } else {
                        mapCharacter[i + 1][j] = mapCharacter[i][j];
                        mapCharacter[i][j] = null;
                    }
                }
                break;
            case "d":
                if (j + 1 < verticalSize) {
                    if (mapCharacter[i][j + 1] != null && mapCharacter[i][j + 1].getFirstWord().equals("b")) {
                        System.out.println("На этой клетке находится противник. Желаете с ним сразиться? (напишите \"yes\" или \"no\")");
                        System.out.print("> ");
                        if (checkingAnswer(in.nextLine())) {
                            if (BotFightTest.fight(character, mapCharacter[i][j + 1])) {
                                mapCharacter[i][j + 1] = mapCharacter[i][j];
                                mapCharacter[i][j] = null;
                            } else {
                                mapCharacter[i][j] = null;
                            }

                        }
                    } else {
                        mapCharacter[i][j + 1] = mapCharacter[i][j];
                        mapCharacter[i][j] = null;
                    }
                }
                break;
        }
    }

    private int findCharacterVertical(Character character) {
        for (int i = 0; i < horizontalSize; i++) {
            for (int j = 0; j < verticalSize; j++) {
                if (mapCharacter[i][j] == character) {
                    return j;
                }
            }
        }
        return -1;
    }

    private int findCharacterHorizontal(Character character) {
        for (int i = 0; i < horizontalSize; i++) {
            for (int j = 0; j < verticalSize; j++) {
                if (mapCharacter[i][j] == character) {
                    return i;
                }
            }
        }
        return -1;
    }

    private boolean checkingAnswer(String str) {
        if (str.toUpperCase().equals("YES")) {
            return true;
        } else if (str.toUpperCase().equals("NO")) {
            return false;
        }

        System.out.println("Неверный формат ответа. Введите заново. \n >");
        return checkingAnswer(in.nextLine());
    }
}
