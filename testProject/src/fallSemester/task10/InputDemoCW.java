package fallSemester.task10;

import java.util.Scanner;

public class InputDemoCW {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String ch;
        boolean run = true;

        while (run) {
            ch = in.nextLine();
            run = !ch.equals("q");
            if(run){
                System.out.println("Input: " + ch.toUpperCase());
            }
        }
        System.out.println("FINISHED");
    }
}
