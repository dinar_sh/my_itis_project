package fallSemester.task10;

import java.util.Scanner;

public class TestDemo {
    private static String questions[] = new String[5];
    private static String answers[][] = new String[5][3];
    private static Scanner in = new Scanner(System.in);
    private static int counterCorrectAnswers = 0;

    public static void main(String[] args) {


        questions[0] = "В каком году была выпущена первая версия Java 1.0?";
        questions[1] = "Какой из этих языков программирования появился первее других?";
        questions[2] = "Сколько будет 2+2?";
        questions[3] = "Сколько будет 3+3";
        questions[4] = "Сколько будет 4+4";

        answers[0][0] = "*1996";
        answers[0][1] = " 1990";
        answers[0][2] = " 1999";

        answers[1][0] = " ALGOL 58";
        answers[1][1] = "*FORTRAN";
        answers[1][2] = " COBOL";

        answers[2][0] = " 0";
        answers[2][1] = "*4";
        answers[2][2] = " -4";

        answers[3][0] = "*6";
        answers[3][1] = " 9";
        answers[3][2] = " 0";

        answers[4][0] = " 4";
        answers[4][1] = " 16";
        answers[4][2] = "*8";
        // звёздочкой помечены верные ответы
        testing();
    }

    public static void testing() {
        String answer = "";
        System.out.println("В тесте представлено всего 5 вопросов c тремя вариантами ответов.");

        for (int i = 0; i < 5; i++) {

            System.out.println("Вопрос №" + (1 + i) + ": " + questions[i]);
            System.out.println("    Варианты ответов: ");

            for (int j = 0; j < 3; j++) {
                System.out.println("                       " + (j + 1) + ")" + answers[i][j].substring(1));
            }

            answer = in.nextLine();

            if (checkingAnswer(i, answer)) {
                counterCorrectAnswers++;
            }

        }
        System.out.println("Количество верных ответов из 5: " + counterCorrectAnswers);
    }


    private static boolean checkingAnswer(int i, String answer) {
        for (int k = 0; k < 3; k++) {
            if ((k + 1) == Integer.parseInt(answer) && answers[i][k].substring(0, 1).equals("*")) {
                return true;
            }
        }
        return false;
    }
}