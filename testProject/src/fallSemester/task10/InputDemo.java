package fallSemester.task10;

import java.util.Scanner;

public class InputDemo {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String name, howAreYou, question1, question2, question3;
        int age;

        //name
        System.out.println("What is your name? (write only your name. Example: Tom)");
        name = in.nextLine();


        //age
        boolean isTrue;
        do {
            isTrue = true;
            System.out.println("How old are you?");
            age = in.nextInt();
            if (age <= 3 || age >= 130) {
                System.out.println("I think it's your fake age. Try again.");
                isTrue = false;
            }
        } while (!isTrue);


        //greetings
        System.out.println("Hi, " + name + "!");
        System.out.println("* please write \"Hi\" or \"Hello\" *");
        do {
            String answer = in.nextLine();
            if (answer.equals("Hi") || answer.equals("Hello")) {
                break;
            }
        } while (true);


        //how are you
        System.out.println("How are you, " + name + "?");
        System.out.println("* please write \"OK\" or \"Bad\" *");
        String answer;
        do {
            answer = in.nextLine();
            if (answer.equals("OK")) {
                System.out.println("Good!");
                break;
            }
            if (answer.equals("Bad")) {
                System.out.println("Sorry :(");
                break;
            }
        } while (true);


        //question?
        String numberOfQuestions;
        do {
            System.out.println("Do you have any questions? ");
            System.out.println("    list of resolved problems:");
            System.out.println("        1. What is my name?");
            System.out.println("        2. How old am I?");
            System.out.println("        3. How am I?");
            System.out.println("        (please, write number of question or write \"No\" if you have not questions)");


            numberOfQuestions = in.nextLine();
            switch (numberOfQuestions) {
                case "1":
                    System.out.println("Your name: " + name);
                    break;
                case "2":
                    System.out.println("Your age" + age);
                    break;
                case "3":
                    System.out.println("You are " + answer);
                    break;
                case "No":
                    System.out.println("Good bye, " + name);
                    break;
            }
        } while (!numberOfQuestions.equals("No"));
    }
}
