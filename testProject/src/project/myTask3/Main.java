package project.myTask3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        File f1 = new File("text1.txt");
        File f2 = new File("text2.txt");

        Symbol symbol = new Symbol(' ', false);
        FileInputStream fileInputStream = new FileInputStream(f1);
        FileOutputStream fileOutputStream = new FileOutputStream(f2);

        InputThread inputThread = new InputThread(symbol, fileInputStream);
        OutputThread outputThread = new OutputThread(symbol, fileOutputStream);

        inputThread.start();
        outputThread.start();

    }
}
