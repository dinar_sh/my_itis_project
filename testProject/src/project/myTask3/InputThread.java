package project.myTask3;

import java.io.FileInputStream;
import java.io.IOException;

public class InputThread extends Thread {
    private Symbol symbol;
    private FileInputStream fileInputStream;

    public InputThread(Symbol symbol, FileInputStream fileInputStream) {
        this.symbol = symbol;
        this.fileInputStream = fileInputStream;
    }


    @Override
    public void run() {
        try {
            while (true) {
                synchronized (symbol) {
                    if (symbol.needToPrint) {
                        symbol.wait();
                    }
                    int c = 0;
                    c = fileInputStream.read();

                    symbol.c = c;
                    if (c != -1) {
                        symbol.needToPrint = true;
                        symbol.notify();
                    } else {
                        symbol.notify();
                        fileInputStream.close();
                        return;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

