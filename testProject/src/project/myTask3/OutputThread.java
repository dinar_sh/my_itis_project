package project.myTask3;

import java.io.FileOutputStream;
import java.io.IOException;

public class OutputThread extends Thread {
    private Symbol symbol;
    private FileOutputStream fileInputStream;

    public OutputThread(Symbol symbol, FileOutputStream fileInputStream) {
        this.symbol = symbol;
        this.fileInputStream = fileInputStream;
    }

   @Override
    public void run() {
        try {
            while (true) {
                synchronized (symbol) {
                    if (!symbol.needToPrint) {
                        symbol.wait();
                    }
                    if(symbol.c == -1) return;
                    fileInputStream.write((char)symbol.c);
                    symbol.needToPrint = false;
                    symbol.notify();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
