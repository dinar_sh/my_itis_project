package controlnayaRabota.task02;

public class Institute {
    private String name;
    private int numberOfFaculties;
    private int numberOfStudents;

    public Institute(String name, int numberOfFaculties, int numberOfStudents) {
        this.name = name;
        this.numberOfFaculties = numberOfFaculties;
        this.numberOfStudents = numberOfStudents;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfFaculties() {
        return numberOfFaculties;
    }

    public void setNumberOfFaculties(int numberOfFaculties) {
        this.numberOfFaculties = numberOfFaculties;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public void setNumberOfStudents(int numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
    }
}

