package controlnayaRabota.task01;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Predicate;

public class TextAnalyzer {
    public static Scanner sc;

    public static void main(String[] args) throws FileNotFoundException {
        sc = new Scanner(new FileInputStream("words.txt"));
        System.out.println(analyze(s -> s.charAt(0) == 's'));           //пример предиката
    }

    private static List<String> analyze(Predicate<String> predicate) {
        Map<String, Integer> map = new TreeMap<>();
        String str = "";
        int count = 0;
        while (sc.hasNext()) {
            str = sc.next();
            if (predicate != null) {
                if (predicate.test(str)) {
                    count = map.getOrDefault(str, 0) + 1;
                    map.put(str, count);
                }
            } else {
                count = map.getOrDefault(str, 0) + 1;
                map.put(str, count);
            }

        }
        List<String> list = new ArrayList<>(map.keySet());
        list.sort((o1, o2) -> map.get(o2) - map.get(o1));
        return list;
    }
}
