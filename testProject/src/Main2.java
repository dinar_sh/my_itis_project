import java.util.StringTokenizer;

public class Main2 {
    public static void main(String[] args) {
        String expr = "(128*2 + 64*2) - 4*(12+4)";
        StringTokenizer tokenizer = new StringTokenizer(expr,"()+-*/",true);
        while (tokenizer.hasMoreTokens()){
            System.out.print(tokenizer.nextToken().trim()+" ");
        }
    }
}
