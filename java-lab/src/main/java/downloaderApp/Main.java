package downloaderApp;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.TreeSet;


public class Main {
    public static void main(String[] args) throws MalformedURLException {
        TreeSet<String> treeSet = new TreeSet<>();
        for (int i = 0; i < args.length; i++) {
            String filename = args[i].substring(args[i].lastIndexOf('/') + 1, args[i].lastIndexOf('.'));
            String suffix = args[i].substring(args[i].lastIndexOf('.'));

            int j = 1;
            boolean var = treeSet.contains(filename + suffix);
            String name = filename;
            while (var) {
                name = filename + (j++);
                var = treeSet.contains(name + suffix);
            }
            treeSet.add(name + suffix);
            File outFile = new File(name + suffix);
            boolean var2 = outFile.exists();
            while (var2) {
                treeSet.add(name + suffix);
                name = filename + (j++);
                outFile = new File(name + suffix);
                var2 = outFile.exists();
            }
            treeSet.add(name + suffix);

            Thread thread = new Thread(new Downloader(new URL(args[i]), outFile));
            thread.start();
        }
    }

    public static void start(String url) throws MalformedURLException {
        main(new String[]{url});
    }
}
