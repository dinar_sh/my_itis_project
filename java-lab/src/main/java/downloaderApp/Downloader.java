package downloaderApp;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class Downloader implements Runnable {
    private URL urlAddress;
    private File outFile;

    public Downloader(URL urlAddress, File outFile) throws MalformedURLException {
        this.urlAddress = urlAddress;
        this.outFile = outFile;
    }

    @Override
    public void run() {
        try (BufferedInputStream in = new BufferedInputStream(urlAddress.openConnection().getInputStream());
             BufferedOutputStream fout = new BufferedOutputStream(new FileOutputStream(this.outFile), 1024)) {
            byte[] buffer = new byte[1024];
            int read = 0;
            while ((read = in.read(buffer, 0, 1024)) >= 0) {
                fout.write(buffer,0,read);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
