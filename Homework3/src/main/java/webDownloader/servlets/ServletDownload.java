package webDownloader.servlets;

import webDownloader.API.DownloadFromURL;

import java.io.IOException;
import java.io.PrintWriter;

public class ServletDownload extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String url = request.getParameter("url");
        DownloadFromURL.download(url);
        doGet(request, response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        PrintWriter pw = response.getWriter();
        pw.println("<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Downloader</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<form action=\"\" method=\"post\">\n" +
                "    <input name=\"url\" type=\"text\">\n" +
                "    <input type=\"submit\">\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>");
    }
}
