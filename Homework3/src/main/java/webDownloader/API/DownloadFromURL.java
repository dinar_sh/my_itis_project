package webDownloader.API;

import downloaderApp.*;

import java.net.MalformedURLException;

public class DownloadFromURL {
    public static void download(String url) throws MalformedURLException {
        Main.start(url);
    }
}
