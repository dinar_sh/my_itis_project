package webDownloader.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.Inet4Address;
import java.net.URL;
import java.util.Date;

public class LogFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        File file = new File("homework3.txt");
        HttpServletRequest request = (HttpServletRequest) req;
        PrintWriter pw = new PrintWriter(new FileWriter(file, true));


        String ipaddress = "";
        try
        {
            URL url_name = new URL("http://bot.whatismyipaddress.com");

            BufferedReader sc =
                    new BufferedReader(new InputStreamReader(url_name.openStream()));

            // reads system IPAddress
            ipaddress = sc.readLine().trim();
        }
        catch (Exception e)
        {
            ipaddress = "Cannot Execute Properly";
        }


        pw.println(new Date() + "   " + request.getMethod() + "   " + request.getParameter("url") + "   " + ipaddress + "             " + request.getRemoteAddr());

        pw.flush();

        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
