package previeous;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class servlet_table extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestTable.tablePost(request, response);
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestTable.tableGet(request, response);
        PrintWriter pw = response.getWriter();
        pw.println("<html>\n" +
                "<head>\n" +
                "    <title></title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<table>\n" +
                "    <tr>\n" +
                "        <th scope=\"col\"> ID</th>\n" +
                "        <th scope=\"col\"> Name</th>\n" +
                "        <th scope=\"col\"></th>\n" +
                "    </tr>");
        for (Good good : RequestTable.goods) {
            pw.println("<tr>\n" +
                    "        <form action=\"./table\" method=\"post\">\n" +
                    "            <td>\n" +
                    good.getId() +
                    "            </td>\n" +
                    "            <td>\n" +
                    "                <label for=\"" + good.getId() + "\">" + good.getName() + "</label>\n" +
                    "            </td>\n" +
                    "            <td>\n" +
                    "                <input type=\"submit\" value=\"Add\" id=\" " + good.getId() + "\" name=\"" + good.getId() + "\"" + ">\n" +
                    "            </td>\n" +
                    "        </form>\n" +
                    "    </tr>");
        }
        pw.println("</table>\n");
        pw.println("<hr>\n" +
                "<table>\n" +
                "    <thead>\n" +
                "    <tr>\n" +
                "        <th colspan=\"4\">Your shopping</th>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <th>ID</th>\n" +
                "        <th>Name</th>\n" +
                "        <th>Quantity</th>\n" +
                "        <th></th>\n"+
                "    </tr>\n" +
                "    </thead>");

        for (Good good : RequestTable.goods) {
            if (good.count > 0) {
                pw.println("<tr>\n" +
                        "       <form action=\"./table\" method=\"post\">\n" +
                        "           <td>"+good.getId()+"</td>\n" +
                        "            <td>\n" +
                        "                <label for=\"" + good.getId() + "\">" + good.getName() + "</label>\n" +
                        "            </td>\n" +
                        "           <td>"+good.getCount()+"</td>\n" +
                        "           <td><input type=\"submit\" value=\"Remove\" name=\""+good.getId()+"\"</td>"+
                        "       </form>\n" +
                        "    </tr>");
            }
        }
        pw.println("</table>\n");

        pw.println("</body>\n" +
                "</html>");
    }
}