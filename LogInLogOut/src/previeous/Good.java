package previeous;

public class Good {
    public static Integer current_id = 0;
    public String name;
    public Integer id;
    public Integer count;

    public Good(String name, Integer id) {
        this.name = name;
        this.id = id;
        this.count = 0;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public Integer getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "Good{" +
                "name='" + name + '\'' +
                ", count=" + count +
                '}';
    }
}
