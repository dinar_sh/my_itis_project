package previeous;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class RequestManager {
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";


    static void loginGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        HttpSession session = request.getSession(false);

        if (session != null) {
            if (session.getAttribute("name") != null) {
                response.sendRedirect("./profile");
            }
            session.invalidate();
        }
        session = request.getSession(true);

        for (Cookie cookie : request.getCookies()) {
            if (cookie.getName().equals("name")) {
                session.setAttribute("name", cookie.getValue());
                response.sendRedirect("./profile");
            }
        }

    }

    static void loginPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        if (request.getParameter("login").equals(LOGIN) && request.getParameter("password").equals(PASSWORD)) {
            HttpSession session = request.getSession(true);
            if (request.getParameter("remember") != null) {
                Cookie cookie = new Cookie("name", request.getParameter("login"));
                cookie.setMaxAge(3600);
                response.addCookie(cookie);
            }
            session.setAttribute("name", request.getParameter("login"));
            response.sendRedirect("./profile");
        } else {
            loginGet(request, response);
        }
    }

    static Object[] profileGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        if (request.getSession(false) != null && request.getSession(false).getAttribute("name") != null) {
            Object[] arguments = new Object[1];
            arguments[0] = request.getSession().getAttribute("name");
            return arguments;
        } else {
            response.sendRedirect("./login");
            return null;
        }
    }

    static void profilePost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    static void logoutGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("name") != null) {
            session.invalidate();
            for (Cookie cookie :
                    request.getCookies()) {
                if (cookie.getName().equals("name")) cookie.setMaxAge(0);
            }
            RequestTable.goods = null;
        } else {
            response.sendRedirect("./login");

        }
    }

    static void logoutPost(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }


}
