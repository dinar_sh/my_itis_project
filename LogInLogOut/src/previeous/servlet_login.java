package previeous;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;

//LOGIN

public class servlet_login extends javax.servlet.http.HttpServlet {

    private MessageFormat format = new MessageFormat("<html>\n" +
            "<head>\n" +
            "    <title>Login</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "<head>\n" +
            "    <title>Login</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "<form name=\"form1\" method=\"post\" action=\"./login\">\n" +
            "    <label for=\"login\">Login</label>\n" +
            "    <input id=\"login\" type=\"text\" name=\"login\" required>\n" +
            "    <label for=\"password\">Password</label>\n" +
            "    <input id=\"password\" type=\"password\" name=\"password\" required>\n " +
            "    <br>" +
            "    <label for=\"remember\">Remember me</label>\n" +
            "    <input type=\"checkbox\" id=\"remember\" name=\"remember\" value=\"remember\">" +
            "    <input type=\"submit\" value=\"Go\">\n" +
            "</form>\n" +
            "</body>\n" +
            "</body>\n" +
            "</html>");

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        RequestManager.loginPost(request,response);

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        RequestManager.loginGet(request,response);

        PrintWriter printWriter = response.getWriter();
        printWriter.println(format.format(null));
    }
}
