package previeous;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;

//PROFILE

public class servlet_profile extends javax.servlet.http.HttpServlet {
    private MessageFormat format = new MessageFormat("<html>\n" +
            "<head>\n" +
            "    <title>Profile</title>\n" +
            "</head>\n" +
            "<h1>Hi! {0}.</h1> This is your profile\n" +
            "<form action=\"./logout\">\n" +
            "    <input type=\"submit\" value=\"Exit\">\n" +
            "</form>\n" +
            "</body>\n" +
            "</html>");

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Object[] arr = RequestManager.profileGet(request, response);
        if (arr != null) {
            PrintWriter printWriter = response.getWriter();
            printWriter.println(format.format(arr));
        }
    }
}
