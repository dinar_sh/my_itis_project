import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        checkConnection();
    }

    private static final String URL_ADDRESS = "jdbc:mysql://localhost:3306/webapp_db?serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASSWORD = "ifrehjdlbyfh";
    private static Connection connection;

    private static void checkConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(URL_ADDRESS, USER, PASSWORD);
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
