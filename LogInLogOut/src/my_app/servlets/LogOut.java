package my_app.servlets;

import my_app.AppApi;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class LogOut extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (AppApi.tryLogOut(request, response)) {
            response.getWriter().println("<html>\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">" +
                    "    <title>Login</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<p>You are logged out</p>" +
                    "</body>\n" +
                    "</html>");
        }
    }
}
