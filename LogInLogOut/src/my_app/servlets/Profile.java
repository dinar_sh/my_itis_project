package my_app.servlets;

import my_app.AppApi;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

public class Profile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = AppApi.tryProfileOrTable(request,response);
        if(Objects.isNull(session)){
            return;
        }
        PrintWriter writer = response.getWriter();
        writer.println("<html>\n" +
                "<head>\n" +
                "<meta charset=\"UTF-8\">" +
                "<title>Profile</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<p>Hello, " + session.getAttribute("login") + "</p>\n" +
                "<p>This is your profile</p>\n"+
                "<form action=\"./logout\" method=\"get\">\n" +
                "    <input type=\"submit\" value=\"Exit\"> \n" +
                "</form>\n"+
                "<form action=\"./table\" method=\"get\">\n" +
                "    <input type=\"submit\" value=\"Shop List\"> \n" +
                "</form>\n");
    }
}
