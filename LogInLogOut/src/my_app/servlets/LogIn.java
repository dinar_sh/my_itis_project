package my_app.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import my_app.AppApi;

public class LogIn extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!AppApi.tryLogin(request, response)) {
            response.getWriter().println("<html>\n" +
                    "<head>\n" +
                    "    <title>Login</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<head>\n" +
                    "    <title>Login</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    request.getAttribute("extra_text") +
                    "<form name=\"form1\" method=\"post\" action=\"./login\">\n" +
                    "    <label for=\"login\">Login</label>\n" +
                    "    <input id=\"login\" type=\"text\" name=\"login\" required>\n" +
                    "    <label for=\"password\">Password</label>\n" +
                    "    <input id=\"password\" type=\"password\" name=\"password\" required>\n " +
                    "    <br>" +
                    "    <label for=\"remember\">Remember me</label>\n" +
                    "    <input type=\"checkbox\" id=\"remember\" name=\"remember\" value=\"remember\">" +
                    "    <input type=\"submit\" value=\"Go\">\n" +
                    "</form>\n" +
                    "</body>\n" +
                    "</body>\n" +
                    "</html>");
        }
    }
}
