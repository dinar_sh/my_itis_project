package my_app.servlets;

import my_app.AppApi;
import my_app.util.Good;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;

public class Table extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("good_delete_id") != null) {
            AppApi.delete(Integer.parseInt(request.getParameter("good_delete_id")));
        } else if (request.getParameter("new_good_name") != null && request.getParameter("new_good_cost") != null) {
            AppApi.add(new Good(request.getParameter("new_good_name"), Integer.parseInt(request.getParameter("new_good_cost"))));
        } else if (request.getParameter("max_cost") != null) {

        }
        response.sendRedirect("./table");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = AppApi.tryProfileOrTable(request, response);
        if (Objects.isNull(session)) {
            return;
        }


        PrintWriter writer = response.getWriter();
        writer.println("<html>\n" +
                "<head>\n" +
                "    <title>$Title$</title>\n" +
                "</head>\n" +
                "<body>");
        writer.println("" +
                "<form name=\"form1\" method=\"post\" action=\"./table\">\n" +
                "   <input name=\"new_good_name\" type=\"text\" placeholder=\"Product name\" required>\n" +
                "   <input name=\"new_good_cost\" type=\"number\" placeholder=\"Product cost\" required>\n" +
                "   <input type=\"submit\" value=\"Add\">\n" +
                "</form>");

        List<Good> list = AppApi.getAll();

        writer.println("<hr>" +
                "<table>\n" +
                "    <tr>\n" +
                "        <td>Name</td>\n" +
                "        <td>Cost</td>\n" +
                "        <td></td>\n" +
                "    </tr>");
        for (Good good : list) {
            writer.println("<tr>\n" +
                    "        <td>\n" +
                    good.getName() +
                    "        </td>\n" +
                    "        <td>\n" +
                    good.getCost()+"$" +
                    "        </td>\n" +
                    "        <td>\n" +
                    "            <form action=\"./table\" method=\"post\">\n" +
                    "                <input type=\"hidden\" name=\"good_delete_id\" value=\"" + good.getId() + "\">\n" +
                    "                <input type=\"submit\" value=\"delete\">\n" +
                    "            </form>\n" +
                    "        </td>\n" +
                    "    </tr>");
        }
        writer.println("</table>\n <hr>");

        if (request.getParameter("max_cost") != null) {
            Good good = AppApi.getMaxCost();
            writer.println("<p> The thing with the highest price: " + good.getName() + " " + good.getCost() + "$</p>");

        }
        writer.println("<form action=\"./table\" method=\"get\">\n" +
                "    <input type=\"submit\" name=\"max_cost\" value=\"Show\"></input>\n" +
                "</form>");

        writer.println("</body>\n" +
                "</html>\n");
    }
}
