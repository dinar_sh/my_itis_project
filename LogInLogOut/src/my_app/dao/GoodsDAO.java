package my_app.dao;

import my_app.util.Good;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GoodsDAO {
    private static Connection connection;

    private static final String URL_ADDRESS = "jdbc:mysql://localhost:3306/webapp_db?useUnicode=true&serverTimezone=Europe/Moscow";
    private static final String USER = "root";
    private static final String PASSWORD = "ifrehjdlbyfh";

    private static void checkConnection() {
        if (connection == null) {
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                connection = DriverManager.getConnection(URL_ADDRESS, USER, PASSWORD);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static Optional<Good> getMaxCost() {
        checkConnection();

        Good good = null;
        String sqlQuery = "SELECT * FROM good WHERE cost_good = (SELECT MAX(cost_good) FROM good)";
        try (PreparedStatement ps = connection.prepareStatement(sqlQuery)) {
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    good = rowMapper.mapRow(resultSet);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(good);
    }

    public static void add(Good good) {
        checkConnection();

        String sqlQuery = "INSERT INTO good (name_good, cost_good) VALUES (?,?)";
        try (PreparedStatement ps = connection.prepareStatement(sqlQuery)) {
            ps.setString(1, good.getName());
            ps.setInt(2, good.getCost());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void delete(Integer id) {
        checkConnection();

        String sqlQuery = "DELETE FROM good WHERE id_good = ?";
        try (PreparedStatement ps = connection.prepareStatement(sqlQuery)) {
            ps.setInt(1, id);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<Good> getAll() {
        checkConnection();

        ArrayList<Good> arrayList = null;
        String sqlQuery = "SELECT * FROM good";
        try (PreparedStatement ps = connection.prepareStatement(sqlQuery)) {
            try (ResultSet rs = ps.executeQuery(sqlQuery)) {
                arrayList = new ArrayList<>();
                while (rs.next()) {
                    arrayList.add(rowMapper.mapRow(rs));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    private static RowMapper<Good> rowMapper = new RowMapper<Good>() {
        @Override
        public Good mapRow(ResultSet row) throws SQLException {
            Good good = new Good();
            good.setId(row.getInt("id_good"));
            good.setName(row.getString("name_good"));
            good.setCost(row.getInt("cost_good"));
            return good;
        }
    };
}
