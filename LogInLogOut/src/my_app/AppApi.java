package my_app;

import my_app.util.Authorization;
import my_app.util.Good;
import my_app.util.GoodsManager;
import my_app.util.SessionCookieManager;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class AppApi {
    public static boolean tryLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (SessionCookieManager.isCorrect(request, response)) {
            response.sendRedirect("./profile");
            return true;
        }

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        boolean checkbox = request.getParameter("remember") != null;

        if (Objects.isNull(login) || Objects.isNull(password)) {
            if (Objects.isNull(request.getAttribute("extra_text"))) {
                request.setAttribute("extra_text", "<h3>Welcome</h3>");
            }
            return false;
        }

        if (!Authorization.authorizationIsCorrect(login, password)) {
            request.setAttribute("extra_text", "<h3> Wrong Login or Password </h3>");
            return false;
        }

        HttpSession session = request.getSession(true);
        session.setAttribute("login", login);
        if (checkbox) {
            Cookie cookie = new Cookie("login", login);
            cookie.setMaxAge(3600);
            response.addCookie(cookie);
        }
        response.sendRedirect("./profile");
        return true;
    }

    public static boolean tryLogOut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (SessionCookieManager.isCorrect(request, response)) {
            SessionCookieManager.delete(request, response);
            return true;
        } else {
            response.sendRedirect("./login");
            return false;
        }
    }

    public static HttpSession tryProfileOrTable(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (SessionCookieManager.isCorrect(request, response)) {
            return request.getSession(false);
        }
        response.sendRedirect("./login");
        return null;
    }

    public static void delete(Integer id) {
        GoodsManager.deleteById(id);
    }

    public static void add(Good good) {
        GoodsManager.add(good);
    }

    public static Good getMaxCost() {
        return GoodsManager.getMaxCost();
    }

    public static List<Good> getAll(){
        return GoodsManager.getAll();
    }

}
