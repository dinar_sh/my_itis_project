package my_app.util;

public class Good {
    public static Integer current_id = 0;
    private Integer id;
    private String name;
    private Integer cost;

    public Good() {
    }

    public Good(String name, Integer cost) {
        this.name = name;
        this.cost = cost;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
