package my_app.util;

public class Authorization {
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "admin";

    public static boolean authorizationIsCorrect(String login, String password) {
        return login.equals(LOGIN) && password.equals(PASSWORD);
    }
}
