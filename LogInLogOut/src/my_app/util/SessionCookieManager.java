package my_app.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionCookieManager {
    public static boolean isCorrect(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("login") != null) {
            return true;
        }

        /*if (session != null && session.getAttribute("login") == null) {
            session.invalidate();
        }*/

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("login")) {
                    /*session = request.getSession(false);*/
                    session.setAttribute("login", cookie.getValue());
                    return true;
                }
            }
        }
        return false;
    }


    public static void delete(HttpServletRequest req, HttpServletResponse resp) {
        req.getSession(false).invalidate();
        Cookie cookie = new Cookie("login", null);
        cookie.setMaxAge(0);
        resp.addCookie(cookie);
    }
}
