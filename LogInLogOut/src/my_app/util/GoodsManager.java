package my_app.util;


import my_app.dao.GoodsDAO;

import java.util.List;
import java.util.Optional;

public class GoodsManager {

    public static void deleteById(Integer delete_id) {
        GoodsDAO.delete(delete_id);
    }

    public static List<Good> getAll() {
        return GoodsDAO.getAll();
    }

    public static Good getMaxCost() {
        Optional<Good> goodOptional = GoodsDAO.getMaxCost();
        if (goodOptional.isPresent()) {
            return goodOptional.get();
        } else {
            return null;
        }
    }

    public static void add(Good good) {
        GoodsDAO.add(good);
    }
}
