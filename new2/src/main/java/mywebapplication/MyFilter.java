package mywebapplication;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class MyFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String method = httpServletRequest.getMethod();
        File f = new File("myfile.txt");
        PrintWriter pw = new PrintWriter(f);
        pw.println(method);
        pw.flush();
        filterChain.doFilter(servletRequest, servletResponse);
/*        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        PrintWriter respWriter = resp.getWriter();
        respWriter.println("hey");*/
    }

    @Override
    public void destroy() {

    }
}
