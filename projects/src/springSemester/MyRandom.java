package springSemester;

public class MyRandom {
    public static long getRandom(long downLim, long upLim) {
        upLim = upLim - downLim+1;
        return (long) (Math.random() * upLim + downLim);
    }
}
