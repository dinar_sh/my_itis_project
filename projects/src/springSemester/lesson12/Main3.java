package springSemester.lesson12;

import java.lang.reflect.Field;

public class Main3 {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Class c = Student.class;
        Field f = c.getDeclaredField("age");
        Student s = new Student(1);
        f.setAccessible(true);
        f.set(s, 90);
        System.out.println(s);

    }
}
