package springSemester.lesson12.superbestawesomeframework;

public class SimpleStudent {
    private int age;
    private String name;

    public SimpleStudent(Integer age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "SimpleStudent{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
