package springSemester.lesson12;

public class Student {

    private int age;
    public String name;

    public Student(int age) {
        this.age = age;
    }

    private void passExam(String exam) {
        System.out.println(exam + " сдал на изи");
        System.out.println("Кстати, я " + name);
    }

    private void passExam(String exam, int countOfTries) {
        System.out.println(exam + " сдал на изи. " + countOfTries + " было попыток");
        System.out.println("Кстати, я " + name);
    }

    private void passExam(String exam, Integer points){
        System.out.println("Сдал " + exam + " на " + points + " баллов.");
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
