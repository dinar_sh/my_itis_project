package springSemester.semestrovochka1;

import java.util.LinkedList;

public class CombSort {

    public static void sort(int[] input) {
        int iter = 0;
        int gap = input.length;
        boolean swapped = true;
        while (gap > 1 || swapped) {
            if (gap > 1) {
                gap = (int) (gap / 1.247330950103979);
            }

            int i = 0;
            swapped = false;
            while (i + gap < input.length) {
                if (input[i] > input[i + gap]) {
                    int t = input[i];
                    input[i] = input[i + gap];
                    input[i + gap] = t;
                    swapped = true;
                }
                i++;
                iter++;
            }
        }
        System.out.println(input.length + " " + iter);
    }

    public static void sort(LinkedList<Integer> e) {
        int[] input = new int[e.size()];
        int k = 0;
        while (e.size() != 0) {
            input[k++] = e.removeFirst();
            e.remove();
        }

        sort(input);

        for (int i = 0; i < input.length; i++) {
            e.addLast(input[i]);
        }
    }

}
