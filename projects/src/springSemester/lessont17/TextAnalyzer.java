package springSemester.lessont17;

public interface TextAnalyzer {
    double analyze(TextProvider tp1, TextProvider tp2);
}
