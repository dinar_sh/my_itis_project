package springSemester.lessont17;

// класс, декорирующий FileTextProvider
public class FileWithNameTextProvider implements TextProvider {
    private FileTextProvider fileTextProvider;

    public FileWithNameTextProvider(FileTextProvider fileTextProvider) {
        this.fileTextProvider = fileTextProvider;
    }

    @Override
    public String getText() {
        String text = fileTextProvider.getText();
        return fileTextProvider.getFile().getName() + " " + text;
    }
}
