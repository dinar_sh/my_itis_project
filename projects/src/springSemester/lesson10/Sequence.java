package springSemester.lesson10;


public class Sequence {
    public static void main(String args[]) {
        permu(0, "ABCD");
    }

    static void permu(int fixed, String s) {
        if (fixed == 0) {
            System.out.println(s);
        }
        if (fixed == s.length()) {
            return;
        }
        char[] chr = s.toCharArray();
        int l = fixed + 1;
        for (int i = l; i < chr.length; i++) {
            swap(chr, fixed, i);
            String st1 = "";
            for (char c : chr) {
                System.out.print(c);
                st1 += c;
            }
            System.out.println();
            permu(fixed + 1, st1);
            swap(chr, i, fixed);
        }
        permu(fixed + 1, s);
    }

    static void swap(char[] ch, int first, int second) {
        char c = ch[first];
        ch[first] = ch[second];
        ch[second] = c;
    }
}
