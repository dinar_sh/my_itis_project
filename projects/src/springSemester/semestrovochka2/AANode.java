package springSemester.semestrovochka2;

// Basic node stored in AVL trees
// Note that this class is not accessible outside
// of package DataStructures

class AANode {
    // Constructors
    AANode(Integer theElement) {
        this(theElement, null, null);
    }

    AANode(Integer theElement, AANode lt, AANode rt) {
        element = theElement;
        left = lt;
        right = rt;
        level = 1;
    }

    // Friendly data; accessible by other package routines
    Integer element;      // The data in the node
    AANode left;         // Left child
    AANode right;        // Right child
    int level;        // Level
}
