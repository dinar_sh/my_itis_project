package springSemester.lesson11;

public interface BST<T extends Comparable> {
    void insert(T elem);
    boolean remove(T elem);
    boolean contains(T elem);
    void printAll();
    void printAllByLevel();

}
