package springSemester.lesson11;

public class Main {
    public static void main(String[] args) {
        BST<Integer> bst = new BinarySearchTree<>();
        bst.insert(20);
        bst.insert(10);
        bst.insert(30);
        bst.insert(23);
        bst.insert(21);
        bst.insert(29);
        bst.insert(27);
        bst.insert(28);
        bst.insert(26);
        bst.remove(30);

        bst.printAll();

        System.out.println();
        bst.printAllByLevel();

        System.out.println();


    }
}
