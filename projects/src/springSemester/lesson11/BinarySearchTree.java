package springSemester.lesson11;

import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTree<T extends Comparable<T>> implements BST<T> {
    private Node root;
    private int size;

    private Boolean isLeft = null;
    private Node toDeleteParent;


    public BinarySearchTree() {
        isLeft = null;
    }

    @Override
    public void insert(T elem) {                        //мой цикл
        if (this.root == null) {
            this.root = new Node(elem);
            size++;
        } else {
            Node next = root;
            Node prev = null;

            while (next != null) {
                prev = next;
                if (next.value.compareTo(elem) >= 0) {
                    next = next.left;
                    isLeft = true;
                } else {
                    next = next.right;
                    isLeft = false;
                }
            }

            if (isLeft) {
                prev.left = new Node(elem);
            } else {
                prev.right = new Node(elem);
            }
            isLeft = null;
            size++;
        }
    }

    private Node insert(Node root, T elem) {        //рекурсия (делали в классе)
        if (root == null) {
            root = new Node(elem);
        } else {
            if (root.value.compareTo(elem) >= 0) {
                root.left = insert(root.left, elem);
            } else {
                root.right = insert(root.right, elem);
            }
        }
        return root;
    }

    @Override
    public boolean remove(T elem) {

        if (contains(elem)) {
            if (isLeft == null) {                                               //case when the node is to delete is root
                if (root.left == null && root.right == null) root = null;
                if (root.left != null) {
                    if (root.right != null) {                                   //у родителя есть два ребёнка
                        Node par = getSuccessParent(root.left);
                        if (par == root) {
                            par.right = root.right;
                            root = par;
                        } else {
                            Node parRight = par.right;
                            parRight.right = root.right;
                            par.right = par.right.left == null ? null : parRight.left;

                            par = root.left;
                            root = parRight;
                            root.left = par;
                        }
                    } else {                                                    //у корня по одному ребёнку
                        root = root.left;
                    }
                } else {
                    root = root.right;
                }
                return true;
            }

            Node toDelete = isLeft ? toDeleteParent.left : toDeleteParent.right;
            if (toDelete.left == null && toDelete.right == null) {              //case when the node has no successor
                if (isLeft) {
                    toDeleteParent.left = null;
                } else {
                    toDeleteParent.right = null;
                }

            }

            if (toDelete.left != null) {
                if (toDelete.right != null) {                                    //case when the node has 2 successors
                    Node par = getSuccessParent(toDelete.left);
                    if (par == toDelete.left) {             //case the node has left sub-tree with height = 1
                        par.right = toDelete.right;
                        if (isLeft) {
                            toDeleteParent.left = par;
                        } else {
                            toDeleteParent.right = par;
                        }
                    } else {                                //case the node has left sub-tree with height >= 2
                        Node parRight = par.right;
                        if (isLeft) {
                            parRight.right = toDelete.right;
                            par.right = par.right.left == null ? null : parRight.left;

                            parRight.left = toDelete.left;
                            if (isLeft) {
                                toDeleteParent.left = parRight;
                            } else {
                                toDeleteParent.right = parRight;
                            }
                        }
                    }

                } else {                                                        //case when the node has only left successor
                    if (isLeft) {
                        toDeleteParent.left = toDelete.left;
                    } else {
                        toDeleteParent.right = toDelete.left;
                    }

                }
            } else {                                                            //case when the node has only right successor
                if (isLeft) {
                    toDeleteParent.left = toDelete.right;
                } else {
                    toDeleteParent.right = toDelete.right;
                }

            }
            isLeft = null;
            toDeleteParent = null;
            return true;
        } else {                                                               //case when such element wasn't found
            isLeft = null;
            toDeleteParent = null;
            return false;
        }
    }

    private Node getSuccessParent(Node left) {
        Node next = left;
        Node prev = left;
        while (next.right != null) {
            prev = next;
            next = next.right;
        }
        return prev;
    }

    @Override
    public boolean contains(T elem) {
        isLeft = null;
        Node prev = null;
        Node next = root;
        while (next != null) {
            if (next.value == elem) {
                toDeleteParent = prev;
                return true;
            }
            prev = next;
            if (next.value.compareTo(elem) > 0) {
                next = next.left;
                isLeft = true;
            } else {
                next = next.right;
                isLeft = false;
            }

        }
        toDeleteParent = null;
        isLeft = null;
        return false;
    }

    @Override
    public void printAll() {
        printAll(this.root);
    }

    private void printAll(Node root) {
        if (root == null) return;
        printAll(root.left);
        System.out.print(root.value + " ");
        printAll(root.right);
    }

    @Override
    public void printAllByLevel() {
        Queue<Node> queue = new LinkedList<>();
        Node top = root;
        System.out.print(top.value + " ");
        if (top.left != null) queue.add(top.left);
        if (top.right != null) queue.add(top.right);
        do {

            if (!queue.isEmpty()) top = queue.poll();
            System.out.print(top.value + " ");
            if (top.left != null) queue.add(top.left);
            if (top.right != null) queue.add(top.right);

        } while (!queue.isEmpty());
    }

    private class Node {
        Node left;
        Node right;
        T value;

        Node(T t) {
            this.value = t;
        }
    }


}
