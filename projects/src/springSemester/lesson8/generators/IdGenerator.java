package springSemester.lesson8.generators;

public interface IdGenerator {
    Long getNextId();
}
