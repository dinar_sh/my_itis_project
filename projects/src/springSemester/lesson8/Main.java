package springSemester.lesson8;

import springSemester.lesson8.models.User;
import springSemester.lesson8.services.UsersService;
import springSemester.lesson8.services.UsersServiceImpl;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите имя пользователя");
        String userName = sc.nextLine();
        System.out.println("Введите пароль");
        String password = sc.nextLine();
        User u = new User(userName, password);
        UsersService service = new UsersServiceImpl();

        User createdUser = service.signUp(u);
        System.out.println(createdUser.toString());
    }
}
