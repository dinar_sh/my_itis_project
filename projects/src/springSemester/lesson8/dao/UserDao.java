package springSemester.lesson8.dao;

import springSemester.lesson8.models.User;

public interface UserDao extends CrudDao<User> {
}