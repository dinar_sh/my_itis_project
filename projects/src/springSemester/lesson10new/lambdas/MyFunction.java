package springSemester.lesson10new.lambdas;

public interface MyFunction {
    String process(String s);
}
