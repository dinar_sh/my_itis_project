package springSemester.lesson3.hometask1;

public class Student {
    private String firstName;
    private String lastName;
    private boolean isMale;
    private int groupNumber;

    public Student(String firstName, String lastName, boolean isMale, int groupNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.isMale = isMale;
        this.groupNumber = groupNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setMale(boolean male) {
        this.isMale = male;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Student)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Student student = (Student) obj;
        return (this.getFirstName().equals(student.getFirstName()) && this.getLastName().equals(student.getLastName()) && this.isMale() == student.isMale() && this.getGroupNumber() == student.getGroupNumber());
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = result * 31 + firstName.hashCode();
        result = result * 31 + lastName.hashCode();
        result = result * 31 + groupNumber;
        if (isMale) {
            result += 1;
        }
        return result;
    }
}
