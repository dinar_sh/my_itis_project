package springSemester.lesson3.hometask1;

import java.util.Comparator;

public class ByGroupNumberComporator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o1.getGroupNumber() - o2.getGroupNumber();
    }
}
