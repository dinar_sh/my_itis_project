package springSemester.lesson3.hometask3;

import springSemester.lesson3.hometask2.Map;

import java.util.Comparator;

public class SimpleSortedMap<K extends Comparable<K>, V> implements Map<K, V> {
    private Entry<K, V>[] entries;
    private int n;
    private static int SIZE = 10;
    private Comparator<K> comparator;

    public SimpleSortedMap() {
        entries = new Entry[SIZE];
        this.comparator = new ByKeyComparator<K>();
    }


    private int binarySearch(K key) {
        int first = 0;
        int last = n - 1;
        int mid = 0;

        while (first <= last) {
            mid = (first + last) / 2;
            if (entries[mid].getKey().compareTo(key) < 0) {
                first = mid + 1;
            } else if (entries[mid].getKey().compareTo(key) > 0) {
                last = mid - 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

    @Override
    public void put(K key, V value) {
        int s = binarySearch(key);
        if (s != -1) {
            entries[s].value = value;
        } else {
            int i = 0;
            while (i < n) {
                if (comparator.compare(key, entries[i].getKey()) < 0) {
                    break;
                }
                i++;
            }
            if (n != entries.length) {
                for (int j = n - 1; j >= i; j--) {
                    entries[j + 1] = entries[j];
                }
                entries[i] = new Entry<>(key, value);
            } else {
                Entry<K, V>[] newEntries = (Entry<K, V>[]) new Object[entries.length + entries.length >> 1];
                for (int j = 0; j < i; j++) {
                    newEntries[j] = entries[j];
                }
                newEntries[i] = new Entry<>(key, value);

                for (int j = i + 1; j <= entries.length; j++) {
                    newEntries[j] = entries[j - 1];
                }
                entries = newEntries;
            }
            n++;
        }
    }

    @Override
    public V get(K key) {
        int i = binarySearch(key);
        if (i == -1) {
            System.out.println("No value was found for this key");
            return null;
        }
        return entries[i].getValue();
    }

    class Entry<I extends Comparable<I>, O> {
        private I key;
        private O value;

        public Entry(I key, O value) {
            this.key = key;
            this.value = value;
        }

        public I getKey() {
            return key;
        }

        public O getValue() {
            return value;
        }

    }

    class ByKeyComparator<K extends Comparable<K>> implements Comparator<K> {

        @Override
        public int compare(K o1, K o2) {
            return o1.compareTo(o2);
        }
    }
}
