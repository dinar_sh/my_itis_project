package springSemester.lesson3.audiotracks;

public class AudioTrack implements Comparable<AudioTrack> {
    private String title;
    private String author;
    private int duraiton;

    public AudioTrack(String title, String author, int duraiton) {
        this.title = title;
        this.author = author;
        this.duraiton = duraiton;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getDuraiton() {
        return duraiton;
    }

    public void setDuraiton(int duraiton) {
        this.duraiton = duraiton;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {//излишне, след. проверка делает всё
            return false;
        }
        if (!(obj instanceof AudioTrack)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        AudioTrack at = (AudioTrack) obj;
        return (this.getDuraiton() == at.getDuraiton() && this.getAuthor().equals(at.getAuthor()) && this.getTitle().equals(at.getTitle()));
    }

    @Override
    public int compareTo(AudioTrack o) {
        return this.getTitle().compareTo(o.getTitle());
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + duraiton;
        result = 31 * result + title.hashCode();
        result = 31 * result + author.hashCode();
        return result;
    }
}
