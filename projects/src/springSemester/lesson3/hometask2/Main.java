package springSemester.lesson3.hometask2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        SimpleMap<String, Integer> map = new SimpleMap<>();
        File f = new File("input.txt");
        Scanner sc = new Scanner(f);
        while (sc.hasNext()) {
            String s = sc.next().replaceAll(",", "").replaceAll("\\.", "").replaceAll("!", "");
            Integer key = map.get(s);
            if (key == null) {
                map.put(s, 1);
            } else {
                map.put(s, key + 1);
            }
        }

        EntryIterator iterator = map.iterator();
        while (iterator.hasNext()) {
            SimpleMap.Entry entry = iterator.next();
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }


}
