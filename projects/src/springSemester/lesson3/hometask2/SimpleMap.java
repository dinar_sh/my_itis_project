package springSemester.lesson3.hometask2;

public class SimpleMap<K, V> implements Map<K, V> {
    public static final int SIZE = 10;
    private int n;
    private Entry<K, V>[] entries;

    public SimpleMap() {
        entries = new Entry[10];
    }


    @Override
    public void put(K key, V value) {
        for (int i = 0; i < n; i++) {
            if (entries[i].key.equals(key)) {
                entries[i].value = value;
                return;
            }
        }
        if (n != entries.length) {
            entries[n++] = new Entry<>(key, value);
        } else {
            Entry<K, V>[] newEntries = (Entry<K, V>[]) new Object[entries.length + entries.length >> 1];
            for (int i = 0; i < n; i++) {
                newEntries[i] = entries[i];
            }
            newEntries[n++] = new Entry<>(key, value);
            entries = newEntries;
        }
    }

    @Override
    public V get(K key) {
        for (int i = 0; i < n; i++) {
            if (entries[i].key.equals(key)) {
                return entries[i].value;
            }
        }
        return null;
    }

    class Entry<I, O> {
        private I key;
        private O value;

        public Entry(I key, O value) {
            this.key = key;
            this.value = value;
        }

        public I getKey() {
            return key;
        }

        public O getValue() {
            return value;
        }
    }

    public EntryIterator iterator() {
        return new EntryIteratorImpl();
    }

    class EntryIteratorImpl implements EntryIterator {
        private int i = 0;

        public EntryIteratorImpl() {

        }

        @Override
        public boolean hasNext() {
            return i < n;
        }

        @Override
        public Entry next() {
            return entries[i++];
        }
    }
}
