package springSemester.lesson3.hometask2;

public interface EntryIterator {
    boolean hasNext();

    SimpleMap.Entry next();
}
