package springSemester.lesson9.builder;

public class Main {
    public static void main(String[] args) {
/*
        User.Builder b = new User.Builder();
        b.id(67L).firstname("Dinar").secondname("Shakurov");
*/

        User u = User.builder()
                .id(67L)
                .firstname("Dinar")
                .secondname("Shakurov")
                .build();

    }


}
