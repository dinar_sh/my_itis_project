package springSemester.lesson14.firstexample;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Thread funny = new FunnyThread();
        Thread evil = new EvilThread();

        funny.start();
        evil.start();

        funny.join();
        for (int i = 0; i < 1000; i++) {
            System.out.println(i + ". Main");
        }
    }
}
