package springSemester.lesson6;

public class Main {
    public static void main(String[] args) {

        ProcessingRule r = s -> {
            s.replaceAll(",", "");
            s = s.toUpperCase();
            return s;
        };

    }
}
