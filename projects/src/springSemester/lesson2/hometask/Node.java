package springSemester.lesson2.hometask;

public class Node {
    int value;
    Node next;

    public Node(int value) {
        this.value = value;
    }
}
