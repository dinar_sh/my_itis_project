package springSemester.lesson2;

public interface IntIterator {
    boolean hasNext();

    int next();
}
