package springSemester.lesson2;

public class IntLinkedList implements IntList {
    private Node first;
    private int numberElements;

    @Override
    public void add(int elem) {
        Node newNode = new Node();
        newNode.value = elem;

        if (first != null) {
            Node current = first;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        } else {
            first = newNode;
        }
        numberElements++;
    }

    @Override
    public int get(int index) {
        if (index >= numberElements || index < 0) {
            throw new IndexOutOfBoundsException("No such element with index = " + index);
        } else {
            int i = 0;
            Node current = first;
            while (i != index) {
                i++;
                current = current.next;
            }
            return current.value;
        }
    }

    @Override
    public void remove(int index) {
        if (index >= numberElements || index < 0) {
            throw new IndexOutOfBoundsException("No such element with index = " + index);
        }
        if (index != 0) {
            Node current = first;
            int i = 0;
            while (i < index - 1) {
                current = current.next;
                i++;
            }
            current.next = current.next.next;
        } else {
            first = first.next;
        }
        numberElements--;
    }

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public IntIterator iterator() {
        return new IntIteratorIml();
    }


    class IntIteratorIml implements IntIterator {
        Node current;

        public IntIteratorIml() {
            current = first;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public int next() {
            int valueToReturn = current.value;
            current = current.next;
            return valueToReturn;
        }
    }

    class Node {
        int value;
        Node next;
    }
}
