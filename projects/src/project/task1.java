package project;

import java.io.*;
import java.util.*;
import java.util.function.Predicate;

public class task1 {
    static File f = new File("words.txt");
    private static Scanner sc;

    public static void main(String[] args) throws FileNotFoundException {
        sc = new Scanner(f);

        System.out.println(getList(s -> s.charAt(0) == 's'));
    }

    public static List<String> getList(Predicate<String> predicate) {
        Map<String, Integer> map = new TreeMap<>();
        while (sc.hasNext()) {
            String s = sc.next();
            if (predicate.test(s)) {
                int i = map.getOrDefault(s, 0);
                map.put(s, i + 1);
            }
        }
        List<String> list = new ArrayList<>(map.keySet());

        list.sort((o1, o2) -> map.get(o2) - map.get(o1));
        return list;
    }
}
