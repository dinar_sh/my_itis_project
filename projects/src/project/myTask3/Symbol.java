package project.myTask3;

public class Symbol {
    public int c;
    public boolean needToPrint;

    public Symbol(char c, boolean needToPrint) {
        this.c = c;
        this.needToPrint = needToPrint;
    }
}
