package controlnayaRabota.task03;

import java.io.FileOutputStream;
import java.io.IOException;

public class OutputThread extends Thread {
    private Symbol symbol;
    private FileOutputStream fileOutputStream;

    public OutputThread(Symbol symbol, FileOutputStream fileOutputStream) {
        this.symbol = symbol;
        this.fileOutputStream = fileOutputStream;
    }

    @Override
    public void run() {
        try {
            while (true) {
                synchronized (symbol) {
                    if (!symbol.needToPrint) symbol.wait();

                    if (symbol.c == -1) {
                        fileOutputStream.close();
                        return;
                    }

                    fileOutputStream.write((char) symbol.c);
                    symbol.needToPrint = false;
                    symbol.notify();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
