package controlnayaRabota.task03;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class MainDoCopy {
    public static void main(String[] args) throws FileNotFoundException {
        Symbol symbol = new Symbol(1, false);

        InputThread inputThread = new InputThread(symbol, new FileInputStream("text1.txt"));
        OutputThread outputThread = new OutputThread(symbol, new FileOutputStream("text2.txt"));

        inputThread.start();
        outputThread.start();
    }
}
