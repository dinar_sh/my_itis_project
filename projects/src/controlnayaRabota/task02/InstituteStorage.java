package controlnayaRabota.task02;

import java.util.*;

public class InstituteStorage {
    private List<Institute> institutes;

    public InstituteStorage() {
        institutes = new ArrayList<>();
    }

    public void add(Institute institute) {
        institutes.add(institute);
    }

    public List<Institute> topN(Comparator<Institute> comparator, int n) {
        int size = institutes.size();
        List<Institute> thisList = List.copyOf(institutes);             //создаем копию списка всех Institute (ибо не знаю, нужен ли исходный список в том же порядке, что и был, т.е. при добавлении)
        thisList.sort(comparator);                                      //далее сортируем его компаратором
        return  thisList.subList(0, size <= n ? size : n);

    }

}
