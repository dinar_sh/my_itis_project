package fallSemester;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exam2 {
    public static void main(String[] args) {
        String str1 = "abbbbba";
        String str2 = "abbbbba";

        if (str1.length() < str2.length()) {
            str2 = str2.substring(0, str1.length());
        } else {
            str1 = str1.substring(0, str2.length());
        }

        String str1New = onlyEven(str1);
        String str2New = onlyEven(str2);
        System.out.println(str1New + " " + str2New);

        Pattern vowels = Pattern.compile("[eyuioa]");
        Pattern consonants = Pattern.compile("[qwrtpsdfghjklzxcvbnm]");

        int count = 0;
        Matcher m1 = vowels.matcher(str1New);
        Matcher m2 = vowels.matcher(str2New);

        while (m1.find() && m2.find()) {
            count++;
        }

        m1 = consonants.matcher(str1New);
        m2 = consonants.matcher(str2New);

        while (m1.find() && m2.find()) {
            count++;
        }

        if (count == str1New.length()) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }

    public static String onlyEven(String str) {
        String strNew = "";
        for (int i = 0; i < str.length(); i += 2) {
            strNew += "" + str.charAt(i);
        }
        return strNew;
    }
}
