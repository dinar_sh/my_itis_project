package fallSemester.Task4;

public class TvBox {
    private int numberOfChannel;
    private TV myTV;

    public TvBox() {
        numberOfChannel = 1;
        myTV = new TV();
    }

    public void channelUp() {
        numberOfChannel++;
        if (numberOfChannel > 100) numberOfChannel = 1;
        myTV.showChannelOnScreen(numberOfChannel);
    }

    public void channelDown() {
        numberOfChannel--;
        if (numberOfChannel < 1) numberOfChannel = 100;
        myTV.showChannelOnScreen(numberOfChannel);
    }

    public void volumeUp() {
        myTV.changeVolumeUp();
    }

    public void volumeDown() {
        myTV.changeVolumeDown();
    }

    public void showChannel(){                          //используется только тогда, когда включается TV (т.е. при включении ТВ показывает текущий канал)
        myTV.showChannelOnScreen(numberOfChannel);
    }
}
