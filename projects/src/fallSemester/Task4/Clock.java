package fallSemester.Task4;

public class Clock {
    private int time;
    private Alarm newAlarm;
    private boolean tag;        // добавлен ли будильник

    public Clock() {
        tag = false;
    }

    public void goClock() {
        if (tag) {
            for (time = 1; time <= newAlarm.getTimeAlarm(); time++) {
                System.out.println("Now is the time " + time);
            }
            time--;
            if (time == newAlarm.getTimeAlarm()) {
                newAlarm.alarmNow();
            }
            time = 0;
            tag = false;
        } else {
            for (time = 1; time < 1000; time++) {
                System.out.println("Now is the time " + time);
            }
            time = 0;
        }
    }


    public void newTimeAlarm(Alarm alarm) {
        newAlarm = alarm;
        tag = true;              // будильник добавлен
    }
}
