package fallSemester.Task4;

public class Screen {

    public Screen() {
    }

    public void showChannel(int numberOfChannel) {
        System.out.println("Channel #" + numberOfChannel);
    }

    public void showVolume(int numberOfVolume, boolean tag) {
        if (tag) {
            System.out.println("Volume: " + numberOfVolume + "%");
        } else {
            if (numberOfVolume == 100) {
                System.out.println("Volume is MAX (100%)");
            } else {
                System.out.println("Volume is MIN (0%)");
            }
        }
    }
}
