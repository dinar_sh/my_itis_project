package fallSemester.Task5;

public class Test1 {
    public static void main(String[] args) {

        String name = "John Smith";
        String firsttName = name.substring(0, name.indexOf(' '));
        firsttName = firsttName.toLowerCase();
        System.out.println(firsttName + ".");
        firsttName = firsttName.toUpperCase();
        System.out.println(firsttName + ".");

        String secondName = name.substring(name.indexOf(' ') + 1);
        secondName = secondName.toLowerCase();
        System.out.println(secondName + ".");
        secondName = secondName.toUpperCase();
        System.out.println(secondName + ".");

    }
}
