package fallSemester.Task5;

public class HomeTask3 {
    public static void main(String[] args) {
        String str = "10 mm";
        System.out.println(Integer.parseInt(str.substring(0, str.indexOf(' ')))*1.0 / 10 + " cm");
        System.out.println(Integer.parseInt(str.substring(0, str.indexOf(' ')))*1.0 / 100 + " m");
        System.out.println(Integer.parseInt(str.substring(0, str.indexOf(' ')))*1.0 / 25.4 + " in");
    }

}
