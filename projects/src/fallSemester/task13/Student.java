package fallSemester.task13;

public class Student {
    public void drawSomethingAtTheNote(Placeable placeable, Coords coords) {
        placeable.placeTo(coords);
        System.out.println("Student draws ****** with coords: x: " + coords.getX() + ", y: " + coords.getY() + ".");
    }
}
