package fallSemester.task13;

public interface Placeable {
    void placeTo(Coords coords);

    Coords getPlace();
}
