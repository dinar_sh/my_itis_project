package fallSemester.task13;

public class Ship implements Moveable {
    private int[] currentCoords;

    public Ship() {
        currentCoords = new int[2];
    }

    public void moveTo(int x, int y) {
        currentCoords[0] = x;
        currentCoords[1] = y;
    }

    public int[] getShipCoords() {
        return currentCoords;
    }
}
