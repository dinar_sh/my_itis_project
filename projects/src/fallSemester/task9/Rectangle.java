package fallSemester.task9;

public class Rectangle {

    /*я сделал для length - unchecked
    * для width - checked */

    private double length;
    private double width;

    public void setLength(double length) {
        if (length <= 0) {
            throw new NoLessThanZeroRuntimeException("Wrong length size");
        }
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    public void setWidth(double width) throws NoLessThanZeroCheckedException {
       /* if (width <= 0) {
            throw new NoLessThanZeroRuntimeException("Wrong width size");
        }*/
       //то что выше первоночально было - unchecked, посколько нужен пример checkedException, то я для метода setWidth написал checkedException
        if (width <= 0) {
            throw new NoLessThanZeroCheckedException("Something go wrong");
        }
        this.width = width;
    }

    public double getWidth() {
        return width;
    }
}
