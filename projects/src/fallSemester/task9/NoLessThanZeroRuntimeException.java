package fallSemester.task9;

public class NoLessThanZeroRuntimeException extends RuntimeException {

    public NoLessThanZeroRuntimeException() {
    }

    public NoLessThanZeroRuntimeException(String message) {
        super(message);
    }

    public NoLessThanZeroRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoLessThanZeroRuntimeException(Throwable cause) {
        super(cause);
    }

    public NoLessThanZeroRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
