package fallSemester.finalExam;

public class MyRandom {
    public static int getRandom(int downLim, int upLim) {
        upLim = upLim - downLim + 1;
        return (int) (Math.random() * upLim + downLim);
    }
}
