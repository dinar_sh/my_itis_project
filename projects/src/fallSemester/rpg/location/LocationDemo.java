package fallSemester.rpg.location;

import fallSemester.rpg.Character;
import fallSemester.rpg.RpgBot;

import java.util.Scanner;

public class LocationDemo {
    public static void main(String[] args) {
        Character newCharacter = new Character("Hero");
        Map myMap = new Map(5, 8);
        myMap.addCharacterAtMap(newCharacter, 3, 3);
        myMap.addCharacterAtMap(new RpgBot("Bot Henry"), 1, 1);
        DisplyaMap.show(myMap);

        Scanner in = new Scanner(System.in);
        while (newCharacter.isAlive()) {
            myMap.characterMove(newCharacter, in.nextLine());
            DisplyaMap.show(myMap);
        }
        System.out.println("You lose.");
    }
}
