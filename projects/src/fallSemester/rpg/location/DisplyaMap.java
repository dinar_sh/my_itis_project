package fallSemester.rpg.location;

public class DisplyaMap {
    public static void show(Map map) {
        System.out.print("  | ");
        int m = map.getHorizontalSize();
        int n = map.getVerticalSize();

        for (int i = 97; i < 97 + n; i++) {
            System.out.print((char) i + " | ");
        }
        System.out.println();
        printLine(m+3);

        for (int i = 0; i < m; i++) {
            System.out.print((i + 1) + " | ");
            for (int j = 0; j < n; j++) {
                if (map.whoIsThere(i, j) != null) {
                    if (map.whoIsThere(i, j).getFirstWord().equals("b")) {
                        System.out.print("B | ");
                    } else {
                        System.out.print("C | ");
                    }
                } else {

                    System.out.print("  | ");
                }
            }
            System.out.println();
            printLine(n);
        }
    }

    private static void printLine(int n) {
        for (int i = 0; i < 4 * n + 3; i++) {
            System.out.print("-");
        }
        System.out.println();
    }
}
