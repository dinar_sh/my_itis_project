package fallSemester.rpg;

public class FightTest {
    public static void main(String[] args) {
        Character char1 = new Character("Hero");
        Character char2 = new Character("Evil");

        while (char1.isAlive() && char2.isAlive()) {
            char1.makeHit(char2);
            if (char2.isDead()) {
                System.out.println(char2.getName() + " is dead.");
                break;
            }

            char2.makeHit(char1);
            if (char1.isDead()) {
                System.out.println(char1.getName() + " is dead.");
                break;
            }

        }

    }
}
