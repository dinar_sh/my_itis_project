package fallSemester.rpg;

public class BotFightTest {
    public static void main(String[] args) {
        Character char1 = new Character("Hero");
        RpgBot bot = new RpgBot("Bot Henry");

        char1.info();
        while (char1.isAlive() && bot.isAlive()) {
            char1.makeHit(bot);
            if (bot.isDead()) {
                System.out.println(bot.getName() + " is dead.");
                break;
            }

            bot.makeHit(char1);
            if (char1.isDead()) {
                System.out.println(char1.getName() + " is dead.");
                break;
            }

        }
    }

    public static boolean fight(Character character, Character bot) {
        while (character.isAlive() && bot.isAlive()) {
            character.makeHit(bot);
            if (bot.isDead()) {
                System.out.println("You killed EnemyBot");
                return true;
            }

            bot.makeHit(character);
            if (character.isDead()) {
                System.out.println("Enemy bot killed You. WASTED");
                return false;
            }
        }
        return true;
    }
}
