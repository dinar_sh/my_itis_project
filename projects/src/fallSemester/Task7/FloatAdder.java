package fallSemester.Task7;

public class FloatAdder {
    public static void main(String[] args) {
        System.out.println("sum : " + floatAdderFucntion(args));
    }

    public static float floatAdderFucntion(String[] a) {
        if (a.length <= 1) {
            System.out.println("Error");
            System.exit(0);
        } else {
            float result = 0;
            for (int i = 0; i < a.length; i++) {
                result = result + Float.parseFloat(a[i]);
            }
            result = Math.round(result * 100) / 100f;
            return result;
        }
        return -1;
    }
}
