package fallSemester.task8;

public @interface Size {
    public int length();

    public int width();

    public int high();
}
