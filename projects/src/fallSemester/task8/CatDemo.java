package fallSemester.task8;

public class CatDemo {
    public static void main(String[] args) {
        Cat myCat = new Cat();

        myCat.speak();

        Class cl = myCat.getClass();
        ItIsAnimal an = (ItIsAnimal) cl.getAnnotation(ItIsAnimal.class);

        System.out.println(an.massa());


        @ItIsAnimal(massa = 10)
        class Dog {
        }

        Dog b = new Dog();
        Class clb = b.getClass();
        ItIsAnimal anb = (ItIsAnimal) clb.getAnnotation(ItIsAnimal.class);

        System.out.println("Dog: " + anb.massa());

    }


}
