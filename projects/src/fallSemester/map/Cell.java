package fallSemester.map;

/**
 * Using in MapArea
 * fallSemester.map consists from Cell
 *
 * @see MapArea
 */
public class Cell {
    private char element = '♥';

    /**
     * @param element - Cell stores element
     */
    public Cell(char element) {
        this.element = element;
    }

    public Cell() {
    }

    /**
     * this method draws Cell in console
     */
    public void drawCell() {       //не очень понял, что именно должен делать этот метода
        if (isEmpty()) {
            System.out.print(" " + " " + " |");
        } else {
            System.out.print(" " + element + " |");
        }
    }

    /**
     * @return element which stored in Cell
     */
    public char getObject() {
        return element;
    }

    /**
     * @param element - add element in Cell
     */
    public void setElement(char element) {
        this.element = element;
    }

    /**
     * check if Cell is empty
     *
     * @return true if Cell - empty, else - false
     */
    public boolean isEmpty() {
        if (element == '♥') {
            return true;
        }
        return false;
    }
}
