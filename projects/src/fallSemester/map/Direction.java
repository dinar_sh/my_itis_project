package fallSemester.map;

public enum Direction {

    D(0, 1),
    S(1, 0),
    A(0, -1),
    W(-1, 0);

    private final int x;
    private final int y;

    private Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @return horizontal coordinate direction
     */
    public int getX() {
        return x;
    }

    /**
     * @return vertical coordinate direction
     */
    public int getY() {
        return y;
    }
}
