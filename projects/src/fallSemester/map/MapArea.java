package fallSemester.map;

/**
 * @author Dinar Shakurov
 * MapArea - universal fallSemester.map
 */
public class MapArea {

    private Cell[][] area;
    private int sizeX;      //number of strings in array
    private int sizeY;      //number of columns in array

    /**
     * Creating Map
     *
     * @param sizeX - vertical size fallSemester.map
     * @param sizeY - horizontal fallSemester.map
     * @see Cell
     * area[][] - array of Cell (our fallSemester.map)
     */
    public MapArea(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        area = new Cell[sizeX][sizeY];
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                area[i][j] = new Cell();
            }
        }

    }

    /**
     * @return vertical size fallSemester.map
     */
    public int getSizeX() {
        return sizeX;
    }

    /**
     * @return horizontal size fallSemester.map
     */
    public int getSizeY() {
        return sizeY;
    }

    /*
    public char getCellChar(int i, int j) {
        return area[i][j].getObject();
    }
*/

    /**
     * Method is used to display fallSemester.map in console
     *
     * @return fallSemester.map array
     * @see MapDisplayConsole
     */
    public Cell[][] getArea() {
        return area;
    }

    /**
     * Adds any element at fallSemester.map
     *
     * @param str - input data by user (coordinates)
     * @param chr - symbol which will be displayed on fallSemester.map
     * @return true if successful added element, else - return false
     */
    public boolean addElement(String str, char chr) {
        int[] coords = MapCoords.getCoords(str);

        if (coords[0] == -1) {
            System.out.println("Неверный формат ввода.");
            return false;
        }

        if (coords[0] < sizeX && coords[1] < sizeY) {
            area[coords[0]][coords[1]].setElement(chr);
            return true;
        } else {
            System.out.println("Выход за границы координат.");
            return false;
        }
    }

    /**
     * movements of character on fallSemester.map
     *
     * @param chr - Character which will be move
     * @param str - direction of travel (input data by user)
     */
    public void move(char chr, String str) {
        while (!Movements.move(chr, str, area)) {
        }
    }
}
