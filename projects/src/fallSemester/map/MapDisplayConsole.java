package fallSemester.map;

/**
 * Draws fallSemester.map in console
 */
public class MapDisplayConsole {
    // 1) строки - цифры, столбцы буквы
    // 2) строки - буквы, столбцы - цифры
    // 3) строоки - буквы, столбцы - буквы
    // 4) строки - цифры, столбцы - цифры

    /**
     * this method draws fallSemester.map in console
     *
     * @param sizeX - vertical size fallSemester.map
     * @param sizeY - horizontal size fallSemester.map
     * @param array - fallSemester.map which will be drawn
     */
    public static void print(int sizeX, int sizeY, Cell[][] array) {
        boolean[] arr = MapCoords.returnArray();

        if (arr[0]) {
            if (arr[1]) {
                doPrint1Case(sizeX, sizeY, array);
            } else {
                doPrint2Case(sizeX, sizeY, array);
            }
        } else {
            if (arr[1]) {
                doPrint3Case(sizeX, sizeY, array);
            } else {
                doPrint4Case(sizeX, sizeY, array);
            }
        }
        System.out.println();
    }

    private static void doPrint1Case(int sizeX, int sizeY, Cell[][] array) {
        System.out.print("  | ");
        for (int i = 0; i < sizeY; i++) {
            if (i < 9) {
                System.out.print((i + 1) + " | ");
            } else {
                System.out.print((i + 1) + "| ");
            }
        }

        System.out.println();
        print1(sizeY);
        System.out.println();

        for (int i = 0; i < sizeX; i++) {
            if (i < 9) {
                System.out.print((i + 1) + " |");
            } else {
                System.out.print((i + 1) + "|");
            }
            for (int j = 0; j < sizeY; j++) {
                array[i][j].drawCell();
            }
            System.out.println();
            print1(sizeY);
            System.out.println();
        }
    }

    private static void doPrint2Case(int sizeX, int sizeY, Cell[][] array) {
        System.out.print("  | ");
        for (int i = 65; i < sizeY + 65; i++) {
            System.out.print((char) i + " | ");

        }

        System.out.println();
        print1(sizeY);
        System.out.println();

        for (int i = 0; i < sizeX; i++) {
            if (i < 9) {
                System.out.print((i + 1) + " |");
            } else {
                System.out.print((i + 1) + "|");
            }
            for (int j = 0; j < sizeY; j++) {
                array[i][j].drawCell();
            }
            System.out.println();
            print1(sizeY);
            System.out.println();
        }
    }

    private static void doPrint3Case(int sizeX, int sizeY, Cell[][] array) {
        System.out.print("  | ");
        for (int i = 0; i < sizeY; i++) {
            if (i < 9) {
                System.out.print((i + 1) + " | ");
            } else {
                System.out.print((i + 1) + "| ");
            }
        }

        System.out.println();
        print1(sizeY);
        System.out.println();

        for (int i = 65; i < sizeX + 65; i++) {
            System.out.print((char) i + " |");
            for (int j = 0; j < sizeY; j++) {
                array[i - 65][j].drawCell();
            }
            System.out.println();
            print1(sizeY);
            System.out.println();
        }
    }

    private static void doPrint4Case(int sizeX, int sizeY, Cell[][] array) {
        System.out.print("  | ");
        for (int i = 65; i < sizeY + 65; i++) {
            System.out.print((char) i + " | ");

        }

        System.out.println();
        print1(sizeY);
        System.out.println();

        for (int i = 65; i < sizeX + 65; i++) {
            System.out.print((char) i + " |");
            for (int j = 0; j < sizeY; j++) {
                array[i - 65][j].drawCell();
            }
            System.out.println();
            print1(sizeY);
            System.out.println();
        }
    }

    private static void print1(int x) {
        System.out.print("--+");
        for (int i = 0; i < x; i++) {
            System.out.print("---+");
        }
    }
}
