package fallSemester.map;

public class Movements {
    /**
     * @param chr  - Character which will be move
     * @param str  - direction of travel (input data by user)
     * @param area - the fallSemester.map on which the character moves
     * @return - true if movements successful
     */
    public static boolean move(char chr, String str, Cell[][] area) {
        int[] coordsOfChar = findChar(area, chr);
        if (coordsOfChar[0] == -1) {
            System.out.println("This character is not on the fallSemester.map");
            return false;
        }

        switch (str.toUpperCase()) {
            case "W":
                changeCoords(area, Direction.W, coordsOfChar);
                break;
            case "A":
                changeCoords(area, Direction.A, coordsOfChar);
                break;
            case "S":
                changeCoords(area, Direction.S, coordsOfChar);
                break;
            case "D":
                changeCoords(area, Direction.D, coordsOfChar);
                break;
            default:
                System.out.println("To move use the buttons \"W\", \"A\", \"S\", \"D\".");
                return false;
        }
        return true;
    }

    private static int[] findChar(Cell[][] area, char chr) {
        for (int i = 0; i < area.length; i++) {
            for (int j = 0; j < area[0].length; j++) {
                if (!area[i][j].isEmpty()) {
                    if (area[i][j].getObject() == chr) {
                        return new int[]{i, j};
                    }
                }
            }
        }
        return new int[]{-1, -1};
    }

    private static void changeCoords(Cell[][] area, Direction direction, int[] coords) {
        int x = direction.getX();
        int y = direction.getY();
        if (coords[0] + x < 0 || coords[0] + x >= area.length || coords[1] + y < 0 || coords[1] + y >= area[0].length) {
            return;
        }
        area[coords[0] + x][coords[1] + y].setElement(area[coords[0]][coords[1]].getObject());
        area[coords[0]][coords[1]].setElement('♥');
    }

}
