package fallSemester.task11;

public class ClassWoek {
    public static void main(String[] args) {
        int[] array = {5, 7, 8, 10, 15, 2, 3};
        System.out.println(sum(array));
        System.out.println(min(array));
        System.out.println(max(array));
        System.out.println(avg(array));
    }

    public static int sum(int[] array) {
        int i = 0;
        for (int a = 0; a < array.length; a++) {
            i += array[a];
        }
        return i;
    }

    public static int min(int[] array) {
        int m = array[0];
        for (int j = 1; j < array.length; j++) {
            if (array[j] < m) {
                m = array[j];
            }
        }
        return m;
    }

    public static int max(int[] array) {
        int m = array[0];
        for (int j = 1; j < array.length; j++) {
            if (array[j] > m) {
                m = array[j];
            }
        }
        return m;
    }

    public static double avg(int[] array) {
        double average = sum(array) * 1.0 / array.length;
        return average;
    }

}
