package fallSemester.task11;

import java.util.Scanner;

public class Game1 {
    private static char[] symbols = {'~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '+', '=', '?', '<', '>', 'ё'};
    private static char[][] tableForUser = new char[6][6];
    private static char[][] originalTable = new char[6][6];
    private static int[][] flagTable = new int[6][6];
    private static Scanner in = new Scanner(System.in);
    private static int counter = 0;

    public static void main(String[] args) {
        startGame();
    }


    private static void displayTable(char[][] table) {


        System.out.println("   | A | B | C | D | E | F ");

        for (int i = 0; i < 14; i++) {
            System.out.print("--");
        }
        System.out.println();

        for (int i = 0; i < 6; i++) {

            System.out.print((i + 1) + ") | ");

            for (int j = 0; j < 6; j++) {
                System.out.print(table[i][j] + " | ");
            }
            System.out.println();

            for (int k = 0; k < 14; k++) {
                System.out.print("--");
            }
            System.out.println();
        }

    }

    private static boolean gameIsOver() {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                if (tableForUser[i][j] == 'x') {
                    return false;
                }
            }

        }
        return true;
    }

    private static int getSecondIndex(String str) {
        switch (str.toUpperCase()) {
            case "A":
                return 0;
            case "B":
                return 1;
            case "C":
                return 2;
            case "D":
                return 3;
            case "E":
                return 4;
            case "F":
                return 5;
            default:
                return -1;
        }
    }

    private static int getFirstIndex(String str) {
        switch (str) {
            case "1":
                return 0;
            case "2":
                return 1;
            case "3":
                return 2;
            case "4":
                return 3;
            case "5":
                return 4;
            case "6":
                return 5;
            default:
                return -1;
        }
    }

    public static void startGame() {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                tableForUser[i][j] = 'x';
            }
        }

        for (int j = 0; j < 18; j++) {
            for (int i = 0; i < 2; i++) {
                while (true) {
                    int first = RandomRange.getRandom(0, 5);
                    int second = RandomRange.getRandom(0, 5);
                    if (flagTable[first][second] == 0) {
                        flagTable[first][second] = 1;
                        originalTable[first][second] = symbols[j];
                        break;
                    }
                }
            }
        }
        String turn = "";
        int firstIndex1, secondIndex1, firstIndex2, secondIndex2;
        System.out.println("Открой все карточки! Пример ввода координат: 1A или 2F (т.е. сначала пишется цифры потом буква)");
        displayTable(tableForUser);
        while (!gameIsOver()) {
            counter++;
            System.out.print("Your turn: ");

            while (true) {
                turn = in.nextLine();

                if (turn.length() != 2) {
                    System.out.println("Wrong format!");
                    continue;
                }

                firstIndex1 = getFirstIndex(turn.substring(0, 1));
                secondIndex1 = getSecondIndex(turn.substring(1));

                if (firstIndex1 == -1 || secondIndex1 == -1) {
                    System.out.println("Wrong format!");
                    continue;
                }
                break;
            }

            tableForUser[firstIndex1][secondIndex1] = originalTable[firstIndex1][secondIndex1];

            displayTable(tableForUser);

            while (true) {
                turn = in.nextLine();

                if (turn.length() != 2) {
                    System.out.println("Wrong format!");
                    continue;
                }

                firstIndex2 = getFirstIndex(turn.substring(0, 1));
                secondIndex2 = getSecondIndex(turn.substring(1));

                if (firstIndex2 == -1 || secondIndex2 == -1) {
                    System.out.println("Wrong format!");
                    continue;
                }
                break;
            }

            tableForUser[firstIndex2][secondIndex2] = originalTable[firstIndex2][secondIndex2];

            displayTable(tableForUser);

            if (tableForUser[firstIndex1][secondIndex1] != tableForUser[firstIndex2][secondIndex2]) {
                System.out.println("Incorrect");
                tableForUser[firstIndex1][secondIndex1] = 'x';
                tableForUser[firstIndex2][secondIndex2] = 'x';
                displayTable(tableForUser);
            }
        }
        System.out.println("Вы выиграли за "+ counter+" ходов!");
    }
}
