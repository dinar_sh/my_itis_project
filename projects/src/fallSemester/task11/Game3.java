package fallSemester.task11;

import java.util.Scanner;

public class Game3 {
    private static Scanner in = new Scanner(System.in);
    private static String[] str = {"rock", "paper", "scissors"};

    public static void main(String[] args) {
        startGame();
    }

    public static void startGame() {
        String turn = "";
        String botTurn = "";
        while (true) {
            System.out.print("> Your turn: ");
            turn = in.nextLine();
            if (turn.equals(str[0]) || turn.equals(str[1]) || turn.equals(str[2])) {
                botTurn = botTurn();
                System.out.println("> Bot's turn: " + botTurn);
                String nameOfWinner = checkForWinner(turn, botTurn);
                if (nameOfWinner.equals("Ничья. Играйте ещё раз.")) {
                    System.out.println(nameOfWinner);
                } else {
                    System.out.println(nameOfWinner + " победил!");
                    break;
                }
            } else System.out.println("Wrong format");

        }
    }

    private static String checkForWinner(String turn, String botTurn) {
        if (turn.equals(botTurn)) {
            return "Ничья. Играйте ещё раз.";
        } else {
            if (turn.equals(str[0])) {
                if (botTurn.equals(str[1])) {
                    return "Bot";
                } else {
                    return "Ты";
                }
            } else {
                if (turn.equals(str[1])) {
                    if (botTurn.equals(str[0])) {
                        return "Player";
                    } else {
                        return "Ты";
                    }
                } else {
                    if (botTurn.equals(str[0])) {
                        return "Bot";
                    } else {
                        return "Ты";
                    }
                }
            }
        }
    }

    private static String botTurn() {
        return str[RandomRange.getRandom(0, 2)];
    }
}
