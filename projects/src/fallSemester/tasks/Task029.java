package fallSemester.tasks;

import java.util.Scanner;

public class Task029 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите целое числа от 2 до 9: ");
        int k = in.nextInt();
        if (k < 2 || k > 9) {
            System.out.println("Error");
            return;
        }
        System.out.print("Введите число, которе будет переводится в 10-ую систему счисления: ");
        int n = in.nextInt();
        int n_length = (String.valueOf(n)).length();
        int result = 0;
        int var;


        for (int i = 0; i < n_length; i++) {
            var = n % degree(10, i + 1) / degree(10, i);
            if (var >= k) {
                System.out.println("Error");
                return;
            }
            result = result + degree(k, i) * var;
        }
        System.out.println(result);

    }

    public static int degree(int n, int deg) {
        if (deg == 0) return 1;
        return n * degree(n, deg - 1);
    }
}
