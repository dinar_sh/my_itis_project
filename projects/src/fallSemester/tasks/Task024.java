package fallSemester.tasks;

import java.util.Scanner;

public class Task024 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double x = in.nextDouble();
        System.out.println(calcSum(x - 1));
    }

    private static double roundDouble(double a) {
        return (Math.round(a * 1_000_000_000) * 1.0 / 1_000_000_000);
    }

    private static double degree(double n, int deg) {
        if (deg == 1) return n;
        return n * degree(n, deg - 1);
    }

    public static double calcSum(double x) {
        double first = 0;
        double second = 0;
        boolean tag = true;

        for (int i = 1; true; i++) {
            if (tag) {
                first = second + 1.0 / (i * degree(9, i) * degree(x, 2 * i));
                tag = false;
            } else {
                second = first + 1.0 / (i * degree(9, i) * degree(x, 2 * i));
                tag = true;
            }
            if (Math.abs(first - second) < 0.000_000_001) {
                if (tag) {
                    return roundDouble(second);
                } else {
                    return roundDouble(first);
                }
            }
        }
    }
}
