package fallSemester.tasks;

import java.util.Scanner;

public class Task034 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }
        int max = Integer.MIN_VALUE;
        int value;


        for (int i = 2; i < n; i++) {
            value = arr[i - 2] + arr[i - 1] + arr[i];
            if (value > max) {
                max = value;
            }
        }

        System.out.println(max);
    }
}
