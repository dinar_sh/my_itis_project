package fallSemester.tasks;

public class Task019 {

    public static void main(String[] args) {
        int result = 0;
        for (int i = 1; i <= 1_000_000; i++) {
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    result += j;
                }
            }
            result -= i;
            if (result == i) {
                System.out.println(i);
            }
            result = 0;
        }

    }
}
