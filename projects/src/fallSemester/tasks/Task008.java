package fallSemester.tasks;

public class Task008 {
    public static void main(String[] args) {
        int k = 2;
        System.out.println("2 * " + k + " = " + (2 * k));

        System.out.println("3 * " + k + " = " + (3 * k));

        System.out.println("4 * " + k + " = " + (4 * k));

        System.out.println("5 * " + k + " = " + (5 * k));

        System.out.println("6 * " + k + " = " + (6 * k));

        System.out.println("7 * " + k + " = " + (7 * k));

        System.out.println("8 * " + k + " = " + (8 * k));

        System.out.println("9 * " + k + " = " + (9 * k));

        //можно было и цклом, но мне стало скучно.
    }
}
