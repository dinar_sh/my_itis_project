package fallSemester.tasks;

import java.util.Scanner;

public class Task020 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        char a = '*';
        char b = '0';

        int var1 = 0;
        int vat2 = 0;

        for (int i = 0; i < n; i++) {           //верхняя часть ромба
            var1 = n - i;
            vat2 = 2 * i + 1;
            for (int j = 0; j < var1; j++) {
                System.out.print(a);
            }
            for (int k = 0; k < vat2; k++) {
                System.out.print(b);
            }
            for (int j = 0; j < var1; j++) {
                System.out.print(a);
            }
            System.out.print("\n");
        }

        var1 = 2 * n + 1;                       //середина
        for (int i = 0; i < var1; i++) {
            System.out.print(b);
        }

        System.out.print("\n");

        for (int i = n-1; i > -1; i--) {        //нижняя часть ромба
            var1 = n - i;
            vat2 = 2 * i + 1;
            for (int j = 0; j < var1; j++) {
                System.out.print(a);
            }
            for (int k = 0; k < vat2; k++) {
                System.out.print(b);
            }
            for (int j = 0; j < var1; j++) {
                System.out.print(a);
            }
            System.out.println();
        }
    }
}
