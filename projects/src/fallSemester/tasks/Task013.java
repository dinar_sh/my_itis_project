package fallSemester.tasks;

import java.util.Scanner;

public class Task013 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        double result = 1;

        for (int i = 1; i <= n; i++) {
            result = result * (((1.0 * 2 * i) / (2 * i - 1)) * ((1.0 * 2 * i) / (2 * i + 1)));
        }

        System.out.println(result);
    }
}
