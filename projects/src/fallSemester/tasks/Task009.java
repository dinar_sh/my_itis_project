package fallSemester.tasks;

import java.util.Scanner;

public class Task009 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        double x = in.nextDouble();

        if (x > 2) {
            System.out.println("y = " + ((x * x - 1) / (x + 2)));
        } else if (x > 0) {
            System.out.println("y = " + ((x * x - 1) * (x + 2)));
        } else {
            System.out.println("y = " + (x * x * (1 + 2 * x)));
        }
    }
}
