package fallSemester.tasks;

import java.util.Scanner;

public class Task010 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double x = in.nextDouble();
        double y = x > 2 ? ((x * x - 1) / (x + 2)) : (x > 0 ? ((x * x - 1) * (x + 2)) : (x * x * (1 + 2 * x)));

        System.out.println("y = " + y);
    }
}
