package fallSemester.tasks;

public class Task005 {
    public static void main(String[] args) {
        double x = 1;
        double y = 2;
        double z = 3;

        x += 2;
        x *= y;
        x -= z;
        x /= y;
        y *= z;
        x += y;
        System.out.println(x);
    }
}
