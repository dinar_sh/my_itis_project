package fallSemester.task12;

public class Tas12Demo {
    public static void main(String[] args) {
        Car myCar = new Car();
        GasStation gs = new GasStation();

        gs.fill(myCar);
        System.out.println("current fuel: " + myCar.getFuel());

        Driver driver = new Driver();

        driver.drive(myCar, 100);
        System.out.println("distance " + myCar.getDistance());
        System.out.println("current fuel: " + myCar.getFuel());
    }
}
