package fallSemester.Lesson_2_0;

public class Parallelogram {
    private double length;
    private double height;

    private double square;
    private double perimeter;
    private double angle;

    public Parallelogram(double length, double height, double angle) {
        this.length = length;
        this.height = height;
        this.angle = angle;
        square = 0;
        perimeter = 0;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        if (length > 0) {
            this.length = length;
            if (perimeter != 0) {
                perimeter = 0;
            }
            if (square != 0) {
                square = 0;
            }
        } else {
            System.out.println("Length can not be equals 0 or less than 0");
            System.exit(0);
        }
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        if (height > 0) {
            this.height = height;
            if (perimeter != 0) {
                perimeter = 0;
            }
            if (square != 0) {
                square = 0;
            }
        } else {
            System.out.println("Height can not be equals 0 or less than 0");
            System.exit(0);
        }
    }

    public double perimeter() {


        if ((length != 0) && (height != 0)) {
            perimeter = 2 * (length + perimeter);
            return perimeter;
        } else {
            System.out.println("It is impossible to calculate the perimeter");
            System.exit(0);
            return -1;
        }

    }

    public double squareForRectangle() {

        if ((length != 0) && (height != 0)) {
            square = length * height;
            return square;
        } else {
            System.out.println("It is impossible to calculate the square");
            System.exit(0);
            return -1;
        }

    }

    public double squareForParallelogram() {
        if (angle != 0) {
            square = Math.sin(angle) * length * height;
            return square;
        } else {
            System.out.println("It is impossible to calculate the square (angel == 0) ");
            return -1;
        }
    }
}
