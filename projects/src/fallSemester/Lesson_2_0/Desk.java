package fallSemester.Lesson_2_0;

public class Desk {
    // ширина, высота, длина, описать свойства
    private double height_desk;
    private double lenght_desk;
    private double width_desk;


    public Desk() {
        height_desk = 0;
        lenght_desk = 0;
        width_desk = 0;
    }

    public double getWidth_desk() {
        return width_desk;
    }

    public void setWidth_desk(double width_desk) {
        this.width_desk = width_desk;
    }

    public double getHeight_desk() {
        return height_desk;
    }

    public void setHeight_desk(double height_desk) {
        this.height_desk = height_desk;
    }

    public double getLenght_desk() {
        return lenght_desk;
    }

    public void setLenght_desk(double lenght_desk) {
        this.lenght_desk = lenght_desk;
    }

}
