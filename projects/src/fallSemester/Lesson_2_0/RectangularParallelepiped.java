package fallSemester.Lesson_2_0;

public class RectangularParallelepiped {
    private double length;
    private double height;
    private double width;
    private double square;
    private double volume;


    public RectangularParallelepiped() {
        length = 0;
        height = 0;
        width = 0;
        square = 0;
        volume = 0;
    }

    public void setLength(double length) {
        if (length <= 0) {
            System.out.println("Length can not be less than 0 or equals 0");
            System.exit(0);
        } else {
            this.length = length;
            if (square != 0) {
                square = 0;
            }
            if (volume != 0) {
                volume = 0;
            }
        }
    }

    public void setHeight(double height) {
        if (height <= 0) {
            System.out.println("Height can not be less than 0 or equals 0");
            System.exit(0);
        } else {
            this.height = height;
            if (square != 0) {
                square = 0;
            }
            if (volume != 0) {
                volume = 0;
            }
        }
    }

    public void setWidth(double width) {
        if (width <= 0) {
            System.out.println("Width can not be less than 0 or equals 0");
            System.exit(0);
        } else {
            this.width = width;
            if (square != 0) {
                square = 0;
            }
            if (volume != 0) {
                volume = 0;
            }
        }
    }

    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    public double square() {
        if (square != 0) {                  //checks square is calculated yet in order to not to calculate again
            return square;
        } else {
            if (length <= 0) {
                System.out.println("can not calculate the square, because length is less than 0 or equals 0");
                System.exit(0);
            } else {
                if (height <= 0) {
                    System.out.println("can not calculate the square, because height is less than 0 or equals 0");
                    System.exit(0);
                } else {
                    if (width <= 0) {
                        System.out.println("can not calculate the square, because width is less than 0 or equals 0");
                        System.exit(0);
                    } else {
                        square = 2 * (length * height + length * width + width * height);
                        return square;
                    }
                }
            }
            return -1;
        }
    }

    public double volume() {
        if (volume != 0) {                  //checks volume is calculated yet in order to not to calculate again
            return volume;
        } else {
            if (length <= 0) {
                System.out.println("can not calculate the volume, because length is less than 0 or equals 0");
                System.exit(0);
            } else {
                if (height <= 0) {
                    System.out.println("can not calculate the volume, because height is less than 0 or equals 0");
                    System.exit(0);
                } else {
                    if (width <= 0) {
                        System.out.println("can not calculate the volume, because width is less than 0 or equals 0");
                        System.exit(0);
                    } else {
                        volume = height * length * width;
                        return volume;
                    }
                }
            }
            return -1;
        }
    }

}
