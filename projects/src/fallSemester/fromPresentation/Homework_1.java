package fallSemester.fromPresentation;

public class Homework_1 {
    public static void main(String[] args) {
        int number = 0;
        while (++number < 101) {
            if (number % 10 == 0) {
                System.out.print(number + " ");
            }
        }
    }
}
