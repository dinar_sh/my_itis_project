package fallSemester.fromPresentation;

public class BreakDemo {
    public static void main(String[] args) {

        int[] arryaOfInts = {32, 87, 3, 589, 12, 1076, 2000, 8, 622, 127};
        int searchfor = 12;

        int i;
        boolean foundIt = false;

        for (i = 0; i < arryaOfInts.length; i++) {
            if (arryaOfInts[i] == searchfor){
                foundIt = true;
                break;
            }
        }

        if (foundIt){
            System.out.println("Found " + searchfor + " at index " + i);
        } else {
            System.out.println(searchfor + " not in the array");
        }
    }
}
