package fallSemester.task14;

public class Test {
    public static void main(String[] args) {
        Box<String> box = new Box<>();
        box.setItem("hello");
        String str = box.getItem();
        System.out.println(str);


        Box<Box> my1box = new Box<>();
        Box<Box> my2box = new Box<>();
        my2box.setItem(box);
        my1box.setItem(my2box);

        Box<Box> boxT = my1box.getItem();
        System.out.println(boxT.getItem().getItem());


        Box<String> boxx1 = new Box<>();
        boxx1.setItem("boxxxxxxx");
        Box<Box<String>> boxx2 = new Box<>();
        boxx2.setItem(boxx1);
        Box<Box<Box<String>>> boxx3 = new Box<>();
        boxx3.setItem(boxx2);
        System.out.println(boxx3.getItem().getItem().getItem());

    }
}
