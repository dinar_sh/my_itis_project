package fallSemester;

import fallSemester.finalExam.MyRandom;

public class Exam1 {
    public static void main(String[] args) {
        //int[] number = new int[3];
        int[] number = {12422, 482229, 3222224};
        // int[][] digits = new int[3][3];
        int[][] digits = {{1,2,4}, {4,3,5}, {9,9,9}};
        //   setValue(number, digits);

        String[] strings = new String[digits.length];
        String[] columns = new String[digits[0].length];
        setValue2(strings, digits);
        setValue3(columns, digits);

        String[] numbers = new String[number.length];
        setValue4(numbers, number);

        for (int i = 0; i < numbers.length-1; i++) {

            for (int j = 0; j < strings.length; j++) {

                if (isConsist(numbers[i], strings[j])) {

                    for (int k = 0; k < columns.length; k++) {

                        if (isConsist(numbers[i + 1], columns[k])) {
                            System.out.println(true);
                            return;
                        }
                    }
                }
            }
        }

        System.out.println(false);
    }

    public static void setValue(int[] numbers, int[][] digits) {
        for (int i = 0; i < digits.length; i++) {
            for (int j = 0; j < digits[0].length; j++) {
                digits[i][j] = MyRandom.getRandom(0, 9);
            }
        }

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = MyRandom.getRandom(0, 999);
        }
    } // тут просто заполняем массивы рандомными числами

    public static void setValue2(String[] str, int[][] digits) {
        for (int i = 0; i < digits.length; i++) {
            for (int j = 0; j < digits[0].length; j++) {
                str[i] = str[i] + String.valueOf(digits[i][j]);
            }
        }
    } //тут заполняем массив строками (String)

    public static void setValue3(String[] str, int[][] digits) {
        for (int i = 0; i < digits[0].length; i++) {
            for (int j = 0; j < digits.length; j++) {
                str[i] = str[i] + String.valueOf(digits[j][i]);
            }
        }
    } //тут заполняем массив столбцами (String)

    public static void setValue4(String[] str, int[] number) {
        for (int i = 0; i < number.length; i++) {
            str[i] = String.valueOf(number[i]);
        }
    } //тут числа переводят в String

    public static boolean isConsist(String num, String string) {
        int k = num.length();
        String str = string;
        boolean tag = false;
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < str.length(); j++) {
                if (num.charAt(i) == str.charAt(j)) {
                    tag = true;
                    str = str.substring(0, j) + str.substring(j + 1);
                    break;
                }
            }
            if (!tag) {
                return false;
            }
            tag = false;
        }
        return true;
    }
}
