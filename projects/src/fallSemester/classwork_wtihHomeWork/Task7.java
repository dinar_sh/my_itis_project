package fallSemester.classwork_wtihHomeWork;

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int condition = 0;
        int coundDeskToWidth = 0;
        int countDeskToLength = 0;

        System.out.print("Enter the dimensions of room: ");
        int lengthRoom = in.nextInt();
        int widthRoom = in.nextInt();


        if (widthRoom > lengthRoom) {       //swap
            condition = lengthRoom;
            lengthRoom = widthRoom;
            widthRoom = condition;
            condition = 0;
        }

        System.out.print("Enter the dimensions of desk: ");
        int lengthDesk = in.nextInt();
        int widthDesk = in.nextInt();

        if (widthDesk > lengthDesk) {       //swap
            condition = lengthDesk;
            lengthDesk = widthDesk;
            widthDesk = condition;
            condition = 0;
        }

        condition = widthDesk; //distance between desk and other desk (or between desk and wall)
        int distanceLengthLim = lengthRoom - condition; // allowed length
        int distanceWidthLim = widthRoom - condition;   // allowed width
        if (distanceLengthLim <= 0 || distanceWidthLim <= 0) {
            System.out.println("Number of desks that fit into this room: " + coundDeskToWidth);
            System.exit(0);
        }

        //Now we check hom many desks fit along the width of the room
        coundDeskToWidth = distanceWidthLim / (widthDesk + condition);

        //Now we check how many desks fit along the length of the room
        countDeskToLength = distanceLengthLim / (lengthDesk + condition);

        condition = coundDeskToWidth * countDeskToLength;   //number of desks in the room
        System.out.println("The largest possible number of desks in the room: " + condition);
    }
}
