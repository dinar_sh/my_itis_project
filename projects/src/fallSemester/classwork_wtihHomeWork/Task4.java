package fallSemester.classwork_wtihHomeWork;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("До какого числа вести счёт: ");
        int toLim = in.nextInt();
        System.out.println();

        System.out.print("Введите количестве колонок: ");
        int column = in.nextInt();
        System.out.println();

        counting(toLim, column);
    }

    public static void counting(int toLim, int column) {
        for (int i = 1; i <= toLim; i++) {
            for (int j = 0; j < column; j++) {
                if (i < 10) {
                    if (i <= toLim) System.out.print("0" + (i++) + " ");
                } else {
                    if (i <= toLim) System.out.print((i++) + " ");
                }
            }
            System.out.println();
        }
    }
}
