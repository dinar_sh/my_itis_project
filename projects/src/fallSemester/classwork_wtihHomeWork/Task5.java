package fallSemester.classwork_wtihHomeWork;

public class Task5 {
    public static void main(String[] args) {
        int count = 0;
        int variable = 10;
        boolean condition1 = true;
        boolean condition2 = false;

        if (condition1) {
            System.out.println("condition1 is TRUE");
        }
        if (!condition2) {
            System.out.println("condition2 is FALSE");
        }

        System.out.println("Number is: " + count);
        count++;
        System.out.println("Number is: " + count);
        count--;
        System.out.println("Number is: " + count);
        count += variable;
        System.out.println("Number is: " + count);
        count -= variable;
        System.out.println("Number is: " + count);
        count = count - 22;
        System.out.println("Number is: " + count);
        count = 100 / (count + 10);
        System.out.println("Number is: " + count);
        count *= variable;
        count /= variable;

        if (count == variable) {
            System.out.println("count == variable");
        }
        if (count != variable) {
            System.out.println("count != variable");
        }
    }
}
