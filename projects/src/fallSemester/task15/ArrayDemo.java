package fallSemester.task15;

public class ArrayDemo {
    public static void main(String[] args) {
        char[][] a1 = {{' ', ' ', '*'}, {' ', ' ', '*'}, {' ', ' ', '*'}};
        char[][] a2 = {{' ', '*', '*'}, {' ', '*', '*'}, {' ', '*', '*'}};
        char[][] a3 = ArrayJoinIntersect.intersect(a1, a2);
        for (int i = 0; i < a3.length; i++) {
            for (int j = 0; j < a3[0].length; j++) {
                System.out.print(a3[i][j] + " ");
            }
            System.out.println();
        }


        System.out.println();
        a3 = ArrayJoinIntersect.join(a1, a2);
        for (int i = 0; i < a3.length; i++) {
            for (int j = 0; j < a3[0].length; j++) {
                System.out.print(a3[i][j] + " ");
            }
            System.out.println();
        }
    }
}
