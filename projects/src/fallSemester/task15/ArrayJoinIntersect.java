package fallSemester.task15;

public class ArrayJoinIntersect {

    public static char[][] join(char[][] a1, char[][] a2) {
        if (checking(a1, a2)) {
            char[][] array = new char[a1.length][a1[0].length];
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[0].length; j++) {
                    char c = ' ';
                    if (a1[i][j] != ' ') c = a1[i][j];
                    if (a2[i][j] != ' ') c = a2[i][j];
                    array[i][j] = c;
                }
            }
            return array;
        } else return new char[0][0];
    }

    public static char[][] intersect(char[][] a1, char[][] a2) {
        if (checking(a1, a2)) {
            char[][] array = new char[a1.length][a1[0].length];
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[0].length; j++) {
                    if (a1[i][j] == a2[i][j] && a1[i][j] != ' ') {
                        array[i][j] = a1[i][j];
                    } else {
                        array[i][j] = ' ';
                    }
                }
            }
            return array;
        } else return new char[0][0];

    }

    private static boolean checking(char[][] a1, char[][] a2) {
        if (a1.length == a2.length) {
            if (a1[0].length == a2[0].length) {
                return true;
            }
        }
        return false;
    }
}
