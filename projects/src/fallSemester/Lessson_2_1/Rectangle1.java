package fallSemester.Lessson_2_1;

public class Rectangle1 {
    private int length;
    private int width;
    public Point origin;

    public Rectangle1(Point startPoint, int length, int height) {
        this.origin = startPoint;
        this.length = length;
        this.width = height;
    }

    public Rectangle1(int length, int height) {
        this.width = height;
        this.length = length;
        origin = new Point(0, 0);
    }

    public void move(int deltaX, int deltaY) {
        origin.change(deltaX,deltaY);
    }

    public void setWidth(int height) {
        if (height > 0) {
            this.width = height;
        } else {
            System.out.println("Height can not be equals 0 or less than 0");
            System.exit(0);
        }
    }

    public void setLength(int length) {
        if (length > 0) {
            this.length = length;
        } else {
            System.out.println("Length can not be equals 0 or less than 0");
            System.exit(0);
        }
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }


}

