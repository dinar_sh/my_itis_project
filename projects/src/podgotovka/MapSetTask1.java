package podgotovka;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MapSetTask1 {
    public static void main(String[] args){
        Map<String, Integer> map = new HashMap<>();
        Scanner sc = new Scanner(System.in);

        int count = Integer.parseInt(sc.nextLine());
        int k;
        String[] strings = new String[count];

        for (int i = 0; i < count; i++) {
            strings[i] = sc.nextLine();
        }


        for (int i = 0; i < count; i++) {

            if (map.containsKey(strings[i])) {
                k = map.get(strings[i]);
                map.put(strings[i], k + 1);
                System.out.println(strings[i] + k);
            } else {
                map.put(strings[i], 1);
                System.out.println("OK");
            }
        }
    }
}
